# MediaCaster - Server
MediaCaster enables users with an Android device to add audio-visual content such as YouTube videos to a shared playlist that is played by a server.

This repository contains the desktop server application, the Android client and embedded server applications can respectively be found on the [Client](https://gitlab.com/mediacaster/client) and [µServer](https://gitlab.com/mediacaster/microserver) repositories.

This project has been developed as a personal project in 2018. I wanted in particular through this project to improve my Android and C++/Qt programming skills.

![Imgur Image](https://i.imgur.com/HbYBHTg.png)

## Technologies
* C++
* Qt
* QML
* JavaScript

## Getting started
### Prerequisites
* Windows is recommended. The application should also run on Linux and macOS but has not been tested on those platforms.
* [youtube-dl](https://github.com/ytdl-org/youtube-dl) executable must be put in PATH or in the same directory as the server binary file.

To build from sources:
* Qt 5.11.1 or above with QML, Quick, Network, WebEngine and WebChannel modules.
* MSVC 2017 or above for Windows, GCC 5 or above for Linux

### Building
For Windows a package containing the compiled application with all dependencies, including youtube-dl, can be downloaded from the [Releases](https://gitlab.com/mediacaster/server/-/releases) page. If you are on another platform or would like to build from sources instead please follow the instructions below.

Windows:
```cmd
cd src
qmake -config release
nmake release
```

Linux & macOS:
```sh
cd src
qmake -config release
make
```

### Running
In case you use the pre-compiled Windows package, simply run the `MediaCasterServer.exe` file after having extracted the whole archive content. Otherwise, in case you have chosen to compile from source follow the instructions below, after having copied youtube-dl executable in the compilation output folder or having added it in PATH, as stated in the prerequisites.

Windows:
```cmd
cd release
MediaCasterServer.exe
```

Linux & macOS:
```sh
./MediaCasterServer
```

### Creating a Windows application package
After having built the application, a package containing all the required Qt dependencies can be created on Windows.
```cmd
mkdir package
xcopy release/MediaCasterServer.exe package
cd package
windeployqt MediaCasterServer.exe --qmldir ..
```

## Gallery
<img src="https://i.imgur.com/HbYBHTg.png" width="49.5%"/>
<img src="https://i.imgur.com/G1KivzV.png" width="49.5%"/>

## Authors
* ALTENBACH Thomas - @taltenba
