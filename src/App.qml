import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11

ApplicationWindow {
    id: controlWindow
    visible: true
    width: 1200
    height: 700
    minimumWidth: 1200
    minimumHeight: 700
    title: qsTr("MediaCaster Server - Control Panel")

    ControlPage {

    }

    MaterialMessageDialog {
        id: quitAppConfirmationDialog
        title: qsTr("Quit?")
        text: qsTr("Do you really want to quit?")
        width: 400

        footer: DialogButtonBox {
            Button {
                text: qsTr("Yes")
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
            }
            Button {
                text: qsTr("No")
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
            }
        }

        onAccepted: {
            if (playlistServer.isServerRunning)
                playlistServer.stopServer();

            Qt.quit();
        }
    }

    onClosing: {
        if (!playlistServer.isServerRunning && playlist.entryModel.rowCount() === 0) {
            Qt.quit();
            return;
        }

        close.accepted = false;
        quitAppConfirmationDialog.open();
    }

//    Window {
//        property alias playerWindow: playerWindow

//        id: playerWindow
//        visible: true
//        width: 720
//        height: 480
//        title: qsTr("MediaCaster Server - Player")
//        flags: Qt.WindowCancelButtonHint | Qt.WindowCloseButtonHint
//        //flags: Qt.CustomizeWindowHint | Qt.WindowTitleHint | /*Qt.WindowMaximizeButtonHint |*/ Qt.WindowMinimizeButtonHint

//        PlayerPage {

//        }
//    }
}


