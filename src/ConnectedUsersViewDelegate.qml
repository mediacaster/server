import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

Item {
    width: parent.width
    height: 30

    Label {
        id: itemContentLayout
        anchors.fill: parent
        text: userName + " (" + userID + ")"
        color: "#C5E1A5"
        elide: Label.ElideRight
        verticalAlignment: Label.AlignVCenter

        Behavior on opacity {
            NumberAnimation { duration: 50 }
        }
    }

    RowLayout {
        id: itemControlLayout
        height: parent.height
        anchors.right: parent.right
        opacity: 0
        enabled: false

        Button {
            Layout.preferredWidth: 20
            Layout.preferredHeight: parent.height

            Image {
                anchors.fill: parent
                anchors.margins: 2
                fillMode: Image.PreserveAspectFit
                source: "qrc:/img/ic_kick_white_24px.svg"
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            onClicked: {
                playlistServer.kickUser(userName);
            }
        }

        Button {
            Layout.preferredWidth: 20
            Layout.preferredHeight: parent.height

            Image {
                anchors.fill: parent
                anchors.margins: 2
                fillMode: Image.PreserveAspectFit
                source: "qrc:/img/ic_block_white_24px.svg"
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            onClicked: {
                playlistServer.banUser(userName);
            }
        }

        Behavior on opacity {
            NumberAnimation { duration: 50 }
        }
    }

    MouseArea {
        anchors.fill: parent
        width: childrenRect.width
        height: childrenRect.height
        hoverEnabled: true
        propagateComposedEvents: true

        onClicked:  mouse.accepted = false;
        onReleased: mouse.accepted = false
        onDoubleClicked: mouse.accepted = false
        onPositionChanged: mouse.accepted = false
        onPressAndHold: mouse.accepted = false
        onPressed: mouse.accepted = false
        onEntered: {
            itemControlLayout.opacity = 1
            itemControlLayout.enabled = true
            itemContentLayout.opacity = 0.25
        }
        onExited: {
            itemControlLayout.opacity = 0
            itemControlLayout.enabled = false
            itemContentLayout.opacity = 1
        }
    }
}
