import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.11
import com.taltenbach.mediacaster.mediaplatform 1.0
import com.taltenbach.mediacaster.playlistentrywrapper 1.0
import com.taltenbach.mediacaster.playlistenqueuereply 1.0

GridLayout {
    rowSpacing: 20
    columnSpacing: 10
    anchors.fill: parent
    anchors.margins: 15

    MaterialMessageDialog {
        id: stopServerConfirmationDialog
        title: qsTr("Stop server?")
        text: qsTr("If you stop the server all the current users are going to be disconnected.\nDo you want to continue?")
        //icon: StandardIcon.Question
        footer: DialogButtonBox {
            Button {
                text: qsTr("Yes")
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
            }
            Button {
                text: qsTr("No")
                flat: true
                DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
            }
        }

        onAccepted: {
            playlistServer.stopServer();
        }
    }

    StartServerDialog {
        id: startServerDialog
        onStartServerRequested: {
            playlistServer.startServer(portBox.value, adminPassword, skipVoteFavorableThreshold);
            playerWindow.visible = true;
        }
    }

    GroupBox {
        title: qsTr("Server")
        Layout.row: 0
        Layout.column: 0
        Layout.fillWidth: true
        Layout.maximumWidth: 400
        Layout.alignment: GridLayout.Center

        label: GroupBoxTitle { }

        GridLayout {
            anchors.fill: parent

            Label {
                text: qsTr("Status :")
                font.pointSize: 12
                Layout.column: 0
                Layout.row: 0
                Layout.bottomMargin: 12
            }

            Label {
                id: serverStatusLabel
                text: qsTr("Stopped")
                /*anchors.left: ipLabel.left
                anchors.right: ipLabel.right*/
                Layout.fillWidth: true
                horizontalAlignment: Label.AlignHCenter
                font.pointSize: 12
                color: "#F44336"
                Layout.column: 1
                Layout.row: 0
                Layout.bottomMargin: 12
            }

            Label {
                text: qsTr("IP :")
                font.pointSize: 12
                Layout.column: 0
                Layout.row: 1
            }

            Label {
                id: ipLabel
                /*anchors.left: portBox.left
                anchors.right: portBox.right*/
                Layout.fillWidth: true
                text: playlistServer.serverAddress
                horizontalAlignment: Label.AlignHCenter
                font.pointSize: 12
                Layout.column: 1
                Layout.row: 1
            }

            Label {
                text: qsTr("Port :")
                font.pointSize: 12
                Layout.column: 0
                Layout.row: 2
            }

            SpinBox {
                id: portBox
                value: 2195
                from: 1
                to: 65535
                editable: true
                enabled: !playlistServer.isServerRunning
                font.pointSize: 12
                //Layout.bottomMargin: -15
                Layout.column: 1
                Layout.row: 2
                Layout.fillWidth: true
                Layout.alignment: GridLayout.Center
                Layout.maximumWidth: 200
            }

            Button {
                id: startStopServerButton
                text: qsTr("Start server")
                Layout.column: 0
                Layout.row: 3
                Layout.columnSpan: 2
                Layout.fillWidth: true

                onClicked: {
                    if (playlistServer.isServerRunning) {
                        if (connectedUsersModel.count > 0)
                            stopServerConfirmationDialog.open();
                        else
                            playlistServer.stopServer();
                    } else {
                        startServerDialog.open();
                    }
                }
            }
        }
    }

    GroupBox {
        title: qsTr("Users")
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.maximumWidth: 400
        Layout.column: 0
        Layout.row: 1

        label: GroupBoxTitle { }

        TabBar {
            id: bar
            width: parent.width

            TabButton {
                text: qsTr("Connected")
            }

            TabButton {
                text: qsTr("Banned")
            }
        }

        StackLayout {
            width: parent.width
            anchors.top: bar.bottom
            anchors.bottom: parent.bottom
            currentIndex: bar.currentIndex

            ListView {
                clip: true
                delegate: ConnectedUsersViewDelegate { }

                model: ListModel {
                    id: connectedUsersModel
                }

                Connections {
                    target: playlistServer
                    onUserConnected: {
                        connectedUsersModel.append({"userID": userID, "userName": userName});
                    }
                    onUserDisconnected: {
                        var i = 0;

                        while (i < connectedUsersModel.count && connectedUsersModel.get(i).userName !== userName)
                            ++i;

                        connectedUsersModel.remove(i);
                    }
                }
            }

            ListView {
                clip: true

                delegate: BannedUsersViewDelegate { }

                model: ListModel {
                    id: bannedUsersModel
                }

                Connections {
                    target: playlistServer
                    onUserBanned: {
                        bannedUsersModel.append({"userID": userID, "userName": userName});
                    }
                    onUserUnbanned: {
                        var i = 0;

                        while (i < bannedUsersModel.count && bannedUsersModel.get(i).userID !== userID)
                            ++i;

                        bannedUsersModel.remove(i);
                    }
                }
            }
        }

//        GridLayout {
//            rowSpacing: 10
//            columnSpacing: 15
//            anchors.leftMargin: 5
//            anchors.rightMargin: 5
//            anchors.fill: parent

//            Label {
//                id: connectedTitleLabel
//                Layout.row: 0
//                Layout.column: 0
//                Layout.preferredWidth: Math.max(contentWidth, bannedTitleLabel.contentWidth)
//                Layout.alignment: Qt.AlignCenter
//                //color: "#C5E1A5"
//                text: qsTr("Connected")
//                font.bold: true
//                font.pointSize: 12
//            }

//            Label {
//                id: bannedTitleLabel
//                Layout.row: 0
//                Layout.column: 2
//                Layout.preferredWidth: Math.max(contentWidth, connectedTitleLabel.contentWidth)
//                Layout.alignment: Qt.AlignCenter
//                //color: "#FFCC80"
//                text: qsTr("Banned")
//                font.bold: true
//                font.pointSize: 12
//            }

//            ListView {
//                clip: true
//                Layout.fillHeight: true
//                Layout.fillWidth: true
//                Layout.row: 1
//                Layout.column: 0

//                delegate: ConnectedUsersViewDelegate { }

//                model: ListModel {
//                    id: connectedUsersModel
//                }

//                Connections {
//                    target: playlistServer
//                    onUserConnected: {
//                        connectedUsersModel.append({"userID": userID, "userName": userName});
//                    }
//                    onUserDisconnected: {
//                        var i = 0;

//                        while (i < connectedUsersModel.count && connectedUsersModel.get(i).userName !== userName)
//                            ++i;

//                        connectedUsersModel.remove(i);
//                    }
//                }
//            }

//            Rectangle {
//                Layout.preferredWidth: 1
//                Layout.fillHeight: true
//                Layout.column: 1
//                Layout.row: 0
//                Layout.rowSpan: 2
//                color: "gray"
//            }

//            ListView {
//                clip: true
//                Layout.fillHeight: true
//                Layout.fillWidth: true
//                Layout.column: 2
//                Layout.row: 1

//                delegate: BannedUsersViewDelegate { }

//                model: ListModel {
//                    id: bannedUsersModel
//                }

//                Connections {
//                    target: playlistServer
//                    onUserBanned: {
//                        bannedUsersModel.append({"userID": userID, "userName": userName});
//                    }
//                    onUserUnbanned: {
//                        var i = 0;

//                        while (i < bannedUsersModel.count && bannedUsersModel.get(i).userID !== userID)
//                            ++i;

//                        bannedUsersModel.remove(i);
//                    }
//                }
//           }
//        }
    }

    GroupBox {
        title: qsTr("Playlist")
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.column: 1
        Layout.columnSpan: 2
        Layout.row: 0
        Layout.rowSpan: 2

        label: GroupBoxTitle { }

        GridLayout {
            anchors.fill: parent
            rowSpacing: 10
            columnSpacing: 10

            ListView {
                id: playlistView
                clip: true
                Layout.row: 0
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.fillWidth: true
                Layout.fillHeight: true

                highlight: Rectangle {
                   Layout.preferredHeight: 85
                   color: "#444444"
                   radius: 5
                   opacity: 0.7
                   focus: true
                }

                delegate: PlaylistViewDelegate { }

                model: playlist.entryModel

                Connections {
                    target: playerController
                    onCurrentEntryChanged: {
                        playlistView.currentIndex = newIndex;
                    }
                    onOutOfMedia: {
                        playlistView.currentIndex = -1;
                    }
                }
            }

            TextField {
                id: mediaURLField
                selectByMouse: true
                placeholderText: "Youtube or Vimeo video URL"
                Layout.row: 1
                Layout.column: 0
                Layout.fillWidth: true
                Layout.leftMargin: 5
                onAccepted: {
                    enqueueMedia(mediaURLField.text);
                    text = "";
                }
                //validator: RegExpValidator { regExp: /(((https?:\/\/)?(((www.)?youtube.com)|(youtu.be))\/((watch\?v=)|(v\/)|(embed\/))?)|((\\s)?(https?:\/\/)?vimeo.com\/(\\s)?))\w*/ }
            }

            Button {
                text: qsTr("Enqueue")
                Layout.row: 1
                Layout.column: 1
                Layout.rightMargin: 5
                onClicked: {
                    enqueueMedia(mediaURLField.text);
                    mediaURLField.text = "";
                }
            }
        }
    }

    Label {
        text: qsTr("Version ") + appVersion
              + " (<a style=\"color:#DDDDDD;\" href=\"http://thomas.altenbach.free.fr/MediaCaster/Server/changelog.php\">Changelog</a>) - "
              + qsTr("Developed by Thomas ALTENBACH")
        textFormat: Text.RichText
        horizontalAlignment: Label.AlignRight
        verticalAlignment: Label.AlignVCenter
        font.pointSize: 9
        color: "#DDDDDD"
        Layout.row: 2
        Layout.column: 0
        Layout.columnSpan: 3
        Layout.fillWidth: true
        onLinkActivated: Qt.openUrlExternally(link)
    }

    MaterialMessageDialog {
        id: msgDialog
        standardButtons: Dialog.Ok
    }

    Connections {
        target: playlistServer
        onServerStateChanged: {
            if (isRunning) {
                serverStatusLabel.text = qsTr("Running...");
                serverStatusLabel.color = "#4CAF50";
                startStopServerButton.text = qsTr("Stop server");
            } else {
                serverStatusLabel.text = qsTr("Stopped");
                serverStatusLabel.color = "#F44336";
                startStopServerButton.text = qsTr("Start server");
            }
        }
    }

    function enqueueMedia(url) {
        if (url === "")
            return;

        var enqueueReply = playlist.enqueueMedia(url, "<Server>");

        if (enqueueReply === null) {
            displayMsgDialog(qsTr("Error"), qsTr("The specified URL is invalid or not supported."));
        } else {
            enqueueReply.success.connect(onEnqueueSuccessful);
            enqueueReply.invalidPlatformID.connect(onEnqueueInvalidPlatformID);
            enqueueReply.networkError.connect(onEnqueueNetworkError);
            enqueueReply.platformAPIError.connect(onEnqueuePlatformAPIError);
        }
    }

    function onEnqueueSuccessful(reply) {
        reply.destroy(1000);
    }

    function onEnqueueInvalidPlatformID(reply) {
        displayEnqueueError(reply, qsTr("The specified media key does not exist on the given video platform."));
        reply.destroy(1000);
    }

    function onEnqueueNetworkError(reply, error) {
        displayEnqueueError(reply, qsTr("A network error has occured. Error code: %1.").arg(error));
        reply.destroy(1000);
    }

    function onEnqueuePlatformAPIError(reply, httpStatusCode) {
        displayEnqueueError(reply, qsTr("The media platform data API has replied incorrectly. Reply code: %1.").arg(httpStatusCode));
        reply.destroy(1000);
    }

    function displayEnqueueError(enqueueReply, errorStr) {
        displayMsgDialog(qsTr("Error"),
                         qsTr("Unable the add the media ") + enqueueReply.platformKey + "@" + mediaPlatformToString(enqueueReply.videoPlatform) + ":\n"
                         + errorStr);
    }

    function mediaPlatformToString(platform) {
        switch (platform) {
            case MediaPlatform.YOUTUBE:
                return "Youtube";
            case MediaPlatform.VIMEO:
                return "Vimeo";
            default:
                return "?";
        }
    }

    function displayMsgDialog(title, msg/*, icon*/) {
        msgDialog.title = title;
        msgDialog.text = msg;
        //msgDialog.icon = icon;
        msgDialog.open();
    }
}
