import QtQuick 2.11
import QtQuick.Controls 2.4

Label {
    text: parent.title
    x: (parent.width - this.width) / 2
    font.bold: true
    font.pointSize: 14
}
