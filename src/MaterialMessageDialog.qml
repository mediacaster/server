import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.4

Dialog {
    /*enum Icon {
        NoIcon,
        Warning,
        Critical
    }

    property int icon: MaterialMessageDialog.Icon.NoIcon*/
    property alias text: textLabel.text

    id: dialog
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2

    RowLayout {
        spacing: 10

        /*Image {
            id: iconImage
            Layout.column: 0
        }*/

        Label {
            id: textLabel
            font.pointSize: 12
            Layout.column: 1
            Layout.fillWidth: true
        }
    }

    function open() {
        /*switch (icon) {
            case MaterialMessageDialog.Icon.NoIcon:
                iconImage.source = null;
                break;
            case MaterialMessageDialog.Icon.Warning:
                iconImage.source = "qrc:/img/ic_warning_white_36px.svg";
                break;
            case MaterialMessageDialog.Icon.Critical:
                iconImage.source = "qrc:/img/ic_critical_white_36px.svg";
                break;
        }*/

        visible = true;
    }
}
