QT += core qml quick network webengine webchannel

CONFIG += c++11

SOURCES += main.cpp \
    playlistserver.cpp \
    playlistuser.cpp \
    message.cpp \
    clientconnectioninfo.cpp \
    clientconnectiontask.cpp \
    playlist.cpp \
    mediaplatformid.cpp \
    playlistentrymodel.cpp \
    playlistentry.cpp \
    playlistenqueuereply.cpp \
    media.cpp \
    playlistentrywrapper.cpp \
    mediainforetrievalreply.cpp \
    mediainforetriever.cpp \
    vote.cpp \
    checkalivereply.cpp \
    playercontroller.cpp \
    mediastream.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

VERSION = 0.1.0

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS \
           APPLICATION_VERSION=\\\"$$VERSION\\\"


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

HEADERS += \
    playlistserver.h \
    playlistuser.h \
    message.h \
    clientconnectioninfo.h \
    clientconnectiontask.h \
    playlist.h \
    mediaplatformid.h \
    mediaplatform.h \
    playlistentrymodel.h \
    playlistentry.h \
    playlistenqueuereply.h \
    media.h \
    playlistentrywrapper.h \
    mediainforetrievalreply.h \
    mediainforetriever.h \
    vote.h \
    checkalivereply.h \
    playercontroller.h \
    mediastream.h

win32:CONFIG(release, debug|release): LIBS += -LC:/OpenSSL-Win64/lib/VC/ -llibeay32MD -lssleay32MD
else:win32:CONFIG(debug, debug|release): LIBS += -LC:/OpenSSL-Win64/lib/VC/ -llibeay32MDd -lssleay32MDd

INCLUDEPATH += C:/OpenSSL-Win64/include
DEPENDPATH += C:/OpenSSL-Win64/include
