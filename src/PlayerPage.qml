import QtQuick 2.11
import QtQuick.Layouts 1.4
import QtQuick.Window 2.11
import QtQuick.Controls 2.4
import QtWebEngine 1.7
import QtWebChannel 1.0

StackLayout {
    property bool mIsPlayerReady: false

    anchors.fill: parent
    focus: true

    WebChannel {
        id: mediaPlayerChannel
    }

    WebEngineView {
        id: mediaPlayerView
        objectName: "mediaPlayerView"
        opacity: 1
        url: "qrc:/html/media_player.html"
        webChannel: mediaPlayerChannel
        Layout.fillHeight: true
        Layout.fillWidth: true
        /*Layout.row: 0
        Layout.column: 0
        Layout.columnSpan: 2*/

        onLoadingChanged: {
            if (loadRequest.status === WebEngineLoadRequest.LoadSucceededStatus)
            {
                //videoPlayerView.runJavaScript("initPlayers()");
                mIsPlayerReady = true
            }
        }
    }

    Component.onCompleted: {
        mediaPlayerChannel.registerObject("playerController", playerController);
    }

    Connections {
        target: playerController
        onCurrentEntryChanged: {
            if (mIsPlayerReady) {
                mediaPlayerView.runJavaScript("onCurrentEntryChanged();");
                playerWindow.visible = true;
            }
        }
        onOutOfMedia: {
            mediaPlayerView.runJavaScript("onOutOfMedia()");
        }
        onSoughtTo: {
            mediaPlayerView.runJavaScript("onSoughtTo(%1)".arg(time));
        }
        onVolumeChanged: {
            mediaPlayerView.runJavaScript("onVolumeChanged(%1)".arg(volume));
        }
        onDoResume: {
            mediaPlayerView.runJavaScript("resumePlayback()");
        }
        onDoPause: {
            mediaPlayerView.runJavaScript("pausePlayback()");
        }
    }

    Connections {
        target: mediaPlayerView
        onFullScreenRequested: {
            if (playerWindow.visibility === Window.FullScreen) {
                request.toggleOn = false;
                playerWindow.showNormal();
            } else {
                request.toggleOn = true;
                playerWindow.showFullScreen();
            }

            request.accept();
        }
    }

    Action {
        shortcut: "Escape"
        onTriggered: {
            if (playerWindow.visibility === Window.FullScreen)
                playerWindow.showNormal();
        }
    }
}
