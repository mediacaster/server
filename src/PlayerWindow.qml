import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11

Window {
    id: playerWindow
    visible: false
    width: 720
    height: 480
    title: qsTr("MediaCaster Server - Player")
    //flags: Qt.CustomizeWindowHint | Qt.WindowTitleHint | /*Qt.WindowMaximizeButtonHint |*/ Qt.WindowMinimizeButtonHint

    onClosing: {
        close.accepted = false;
        playerWindow.lower();
    }

    PlayerPage {

    }
}
