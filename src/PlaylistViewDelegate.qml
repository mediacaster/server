import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

Item {
    anchors.right: parent.right
    width: parent.width
    height: 85

    Item {
        id: itemInfoLayout
        anchors.fill: parent
        anchors.margins: 5
        anchors.leftMargin: 10

        Image {
            id: mediaThumbnail
            height: 72
            width: 128
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            source: thumbnailURL
            fillMode: Image.PreserveAspectFit
            horizontalAlignment: Image.AlignHCenter
            verticalAlignment: Image.AlignVCenter
        }

        Column {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: mediaThumbnail.right
            anchors.leftMargin: 15

            Label {
                anchors.left: parent.left
                anchors.right: parent.right
                text: title
                maximumLineCount: 2
                elide: Label.ElideRight
                wrapMode: Label.Wrap
                font.bold: true
                font.pointSize: 14
             }

            Label {
                anchors.left: parent.left
                anchors.right: parent.right
                text: qsTr("Added by: ") + adderName
                elide: Label.ElideRight
                font.pointSize: 11
            }
        }

        Behavior on opacity {
            NumberAnimation { duration: 50 }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                playerController.playEntry(index);
            }
        }
    }

    GridLayout {
        id: itemControlLayout
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 5
        anchors.rightMargin: 10
        rowSpacing: 0
        opacity: 0
        enabled: false

        Button {
            Layout.preferredHeight: 35
            Layout.preferredWidth: 35
            Layout.column: 0
            Layout.row: 0
            Layout.alignment: Qt.AlignBottom

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:///img/ic_arrow_up_white_24px.svg"
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            onClicked: {
                playlist.moveEntryUp(index);
            }
        }

        Button {
            Layout.preferredHeight: 35
            Layout.preferredWidth: 35
            Layout.column: 0
            Layout.row: 1
            Layout.alignment: Qt.AlignTop
            Layout.topMargin: -5

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:///img/ic_arrow_down_white_24px.svg"
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            onClicked: {
                playlist.moveEntryDown(index);
            }
        }

        Button {
            Layout.preferredHeight: 65
            Layout.preferredWidth: 35
            Layout.column: 1
            Layout.row: 0
            Layout.rowSpan: 2

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: "qrc:///img/ic_delete_white_24px.svg"
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
            }

            onClicked: {
                playlist.removeEntry(index);
            }
        }

        Behavior on opacity {
            NumberAnimation { duration: 50 }
        }
    }

    MouseArea {
        anchors.fill: parent
        width: childrenRect.width
        height: childrenRect.height
        hoverEnabled: true
        propagateComposedEvents: true
        cursorShape: "PointingHandCursor"

        onClicked:  mouse.accepted = false;
        onReleased: mouse.accepted = false
        onDoubleClicked: mouse.accepted = false
        onPositionChanged: mouse.accepted = false
        onPressAndHold: mouse.accepted = false
        onPressed: mouse.accepted = false
        onEntered: {
            itemControlLayout.opacity = 1
            itemControlLayout.enabled = true
            itemInfoLayout.opacity = 0.25
        }
        onExited: {
            itemControlLayout.opacity = 0
            itemControlLayout.enabled = false
            itemInfoLayout.opacity = 1
        }
    }
}
