import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.4
import Qt.labs.settings 1.0

Dialog {
    signal startServerRequested(string adminPassword, real skipVoteFavorableThreshold)

    id: dialog
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    title: qsTr("Start server")
    focus: true

    Settings {
        id: settings

        property bool skipVoteEnabled: true
        property int skipVoteThreshold: 50
    }

    footer: DialogButtonBox {
        Button {
            id: startButton
            text: qsTr("Start")
            flat: true
            enabled: false
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: qsTr("Cancel")
            flat: true
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
    }

    GridLayout {
        columnSpacing: 15

        CheckBox {
            id: skipVoteCheckBox
            text: qsTr("Enable skip votes")
            font.pointSize: 12
            checked: settings.skipVoteEnabled
            leftPadding: 0
            bottomPadding: 0
            Layout.row: 0
            Layout.column: 0
            Layout.columnSpan: 2
        }

        RowLayout {
            spacing: 10
            Layout.row: 1
            Layout.column: 0
            Layout.columnSpan: 2
            enabled: skipVoteCheckBox.checked

            Label {
                text: qsTr("Favorable votes to skip:")
                font.pointSize: 12
                Layout.column: 0
                Layout.leftMargin: 28
            }

            Slider {
                id: favorableThresholdSlider
                from: 0
                to: 100
                value: settings.skipVoteThreshold
                stepSize: 5
                padding: 0
                Layout.column: 1
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter

                onEnabledChanged: {
                    var overbar = favorableThresholdSlider.background.children[0];
                    var underbar = favorableThresholdSlider.background;
                    var handle = favorableThresholdSlider.handle.children[0];

                    if (enabled) {
                        overbar.color = "#B0BEC5";
                        underbar.color = "#B0BEC5";
                        handle.color = "#B0BEC5";
                    } else {
                        overbar.color = "#6D6D6D";
                        underbar.color = "#6D6D6D";
                        handle.color = "#6D6D6D";
                    }
                }
            }

            Label {
                text: favorableThresholdSlider.value + " %"
                horizontalAlignment: Label.AlignRight
                font.pointSize: 12
                Layout.minimumWidth: 48
                Layout.maximumWidth: 48
                Layout.column: 2
            }
        }

        Label {
            text: qsTr("Admin password :")
            font.pointSize: 12
            Layout.row: 2
            Layout.column: 0
            Layout.alignment: Qt.AlignVCenter
        }

        TextField {
            id: adminPasswordField
            font.pointSize: 12
            selectByMouse: true
            echoMode: "Password"
            focus: true
            Layout.column: 1
            Layout.row: 2
            Layout.minimumWidth: 250
            Layout.fillWidth: true

            onAccepted: {
                if (text != "")
                    dialog.accept();
            }

            onTextChanged: {
                startButton.enabled = text != "";
            }
        }
    }

    onAccepted: {
        settings.skipVoteEnabled = skipVoteCheckBox.checked;
        settings.skipVoteThreshold = favorableThresholdSlider.value;

        startServerRequested(adminPasswordField.text, skipVoteCheckBox.enabled ? (favorableThresholdSlider.value / 100.0) : -1.0);
    }

    function open() {
        adminPasswordField.text = "";

        if (result == Dialog.Rejected) {
            // Cancel changes
            skipVoteCheckBox.checked = settings.skipVoteEnabled;
            favorableThresholdSlider.value = settings.skipVoteThreshold;
        }

        visible = true;
    }
}
