#include "message.h"
#include "checkalivereply.h"

#define REPLY_TIMEOUT 3000

CheckAliveReply::CheckAliveReply(QTcpSocket& socket, QDataStream& socketStream, QObject* parent) : QObject(parent), mSocket(socket), mTimeoutTimer()
{
    if (mSocket.bytesAvailable() > 0)
    {
        emit alive();
        return;
    }

    connect(&mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(&mSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(&mTimeoutTimer, SIGNAL(timeout()), this, SLOT(onTimeout()));

    Message::writeMessage(socketStream, Message::Header::PING);
    socket.flush();

    mTimeoutTimer.setSingleShot(true);
    mTimeoutTimer.start(REPLY_TIMEOUT);
}

void CheckAliveReply::onReadyRead()
{
    mTimeoutTimer.stop();

    emit alive();

    disconnect(&mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void CheckAliveReply::onTimeout()
{
    emit dead();
    disconnect(&mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void CheckAliveReply::onDisconnected()
{
    mTimeoutTimer.stop();
    emit dead();
}
