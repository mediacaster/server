#ifndef CHECKALIVEREPLY_H
#define CHECKALIVEREPLY_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>

class CheckAliveReply : public QObject
{
    Q_OBJECT

    public:
        CheckAliveReply(QTcpSocket& socket, QDataStream& socketStream, QObject* parent = nullptr);

    signals:
        void alive();
        void dead();

    private slots:
        void onReadyRead();
        void onTimeout();
        void onDisconnected();

    private:
        QTcpSocket& mSocket;
        QTimer mTimeoutTimer;
};

#endif // CHECKALIVEREPLY_H
