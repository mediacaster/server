#include "clientconnectioninfo.h"

ClientConnectionInfo::ClientConnectionInfo() : mProtocolVersion(0), mUserName(), mUserID()
{
    // Empty
}

ClientConnectionInfo::ClientConnectionInfo(qint16 protocolVersion, QString const& userName, QString const& userID, QString const& adminPassword)
    : mProtocolVersion(protocolVersion), mUserName(userName), mUserID(userID), mAdminPassword(adminPassword)
{
    // Empty
}

qint16 ClientConnectionInfo::getProtocolVersion() const
{
    return mProtocolVersion;
}

QString const& ClientConnectionInfo::getUserName() const
{
    return mUserName;
}

QString const& ClientConnectionInfo::getUserID() const
{
    return mUserID;
}

QString const& ClientConnectionInfo::getAdminPassword() const
{
    return mAdminPassword;
}

QDataStream& operator<<(QDataStream& out, ClientConnectionInfo const& userConnectionInfo)
{
    out << userConnectionInfo.getProtocolVersion()
        << userConnectionInfo.getUserName()
        << userConnectionInfo.getUserID()
        << userConnectionInfo.getAdminPassword();

    return out;
}

QDataStream& operator>>(QDataStream& in, ClientConnectionInfo& userConnectionInfo)
{
    qint16 protocolVersion;
    QString userName, userID, adminPassword;
    in >> protocolVersion >> userName >> userID >> adminPassword;

    userConnectionInfo = ClientConnectionInfo(protocolVersion, userName, userID, adminPassword);

    return in;
}
