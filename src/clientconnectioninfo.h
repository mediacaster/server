#ifndef CLIENTCONNECTIONINFO_H
#define CLIENTCONNECTIONINFO_H

#include <QDataStream>

class ClientConnectionInfo
{
public:
    ClientConnectionInfo();
    ClientConnectionInfo(qint16 protocolVersion, QString const& userName, QString const& userID, QString const& adminPassword);

    qint16 getProtocolVersion() const;
    QString const& getUserName() const;
    QString const& getUserID() const;
    QString const& getAdminPassword() const;

private:
    qint16 mProtocolVersion;
    QString mUserName;
    QString mUserID;
    QString mAdminPassword;
};

QDataStream& operator<<(QDataStream &out, ClientConnectionInfo const& userConnectionInfo);
QDataStream& operator>>(QDataStream &in, ClientConnectionInfo& ClientConnectionInfo);

#endif // CLIENTCONNECTIONINFO_H
