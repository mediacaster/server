#include <QDataStream>
#include "clientconnectiontask.h"
#include "clientconnectioninfo.h"
#include "playlistserver.h"
#include "message.h"

/**
  * @brief Version of the protocol used to communicate between the playlist client and server
  *        If the version of the client application is different from the server application one,
  *        the applications cannot communicate.
  */
#define PROTOCOL_VERSION 1

/**
  * @brief Maximum duration (in ms) allowed to receive the user information from the client application.
  */
#define USER_INFO_TIMEOUT 5000

ClientConnectionTask::ClientConnectionTask(QTcpSocket& socket, QObject* parent)
    : QObject(parent), mSocket(socket), mSocketStream(&socket), mConnectionTimeoutTimer()
{
    PlaylistServer::setUpDataStream(mSocketStream);
}

void ClientConnectionTask::tryConnect()
{
    if (mConnectionTimeoutTimer.isActive())
        return;

    connect(&mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()), Qt::UniqueConnection);
    connect(&mConnectionTimeoutTimer, SIGNAL(timeout()), this, SLOT(onConnectionTimeout()), Qt::UniqueConnection);

    mConnectionTimeoutTimer.setSingleShot(true);
    mConnectionTimeoutTimer.start(USER_INFO_TIMEOUT);

    if (mSocket.bytesAvailable() > 0)
        onReadyRead();
}

void ClientConnectionTask::onReadyRead()
{
    if (static_cast<size_t>(mSocket.bytesAvailable()) < sizeof(qint16))
        return;

    mSocketStream.startTransaction();

    Message::Header msgHeader = Message::readMessageHeader(mSocketStream);

    if (msgHeader != Message::Header::CLIENT_INFO)
    {
        mSocketStream.abortTransaction();
        mConnectionTimeoutTimer.stop();
        emit connectionFailed(&mSocket, ConnectionError::InvalidInfo);
        return;
    }

    ClientConnectionInfo connectionInfo;
    mSocketStream >> connectionInfo;

    if (!mSocketStream.commitTransaction())
        return;

    mConnectionTimeoutTimer.stop();

    if (connectionInfo.getProtocolVersion() != PROTOCOL_VERSION)
    {
        emit connectionFailed(&mSocket, ConnectionError::IncompatibleProtocolVersion);
        return;
    }

    emit connectionSucceeded(&mSocket, connectionInfo);
}

void ClientConnectionTask::onConnectionTimeout()
{
    emit connectionFailed(&mSocket, ConnectionError::Timeout);
}
