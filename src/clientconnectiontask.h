#ifndef CLIENTCONNECTIONTASK_H
#define CLIENTCONNECTIONTASK_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include "clientconnectioninfo.h"

/**
 * @brief Used to retrieve the information of a newly connected user to the playlist server.
 */
class ClientConnectionTask : public QObject
{
    Q_OBJECT

public:
    ClientConnectionTask(QTcpSocket& socket, QObject* parent = nullptr);

    /**
     * @brief Tries to retrieve the connection information from the user.
     * @param socket The socket connected to the user.
     *
     * If no valid information are sent before the timeout expires or if the protocol
     * version of the client is incompatible with the one of the server, a connectionFailed
     * signal is emitted.
     * Otherwise a connectionSucceeded signal is emitted with the connection information.
     */
    void tryConnect();

    enum class ConnectionError
    {
        Timeout,                        /**< The timeout period elapsed before receiving the user info */
        IncompatibleProtocolVersion,    /**< The client protocol version is incompatible with the one of the server */
        InvalidInfo                     /**< The information sent by the client are invalid */
    };

signals:
    void connectionSucceeded(QTcpSocket* socket, ClientConnectionInfo const& clientConnectionInfo);
    void connectionFailed(QTcpSocket* socket, ClientConnectionTask::ConnectionError error);

private slots:
    void onReadyRead();
    void onConnectionTimeout();

private:
    QTcpSocket& mSocket;
    QDataStream mSocketStream;
    QTimer mConnectionTimeoutTimer;
};

#endif // CLIENTCONNECTIONTASK_H
