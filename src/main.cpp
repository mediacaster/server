#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtWebEngine>
#include <playlist.h>
#include <playlistentrymodel.h>
#include <playercontroller.h>
#include <playlistserver.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL); // Avoid flickering when resizing window (on Windows 10 at least)

    QGuiApplication app(argc, argv);
    app.setOrganizationName("Nablatec");
    app.setOrganizationDomain("thomas.altenbach.free.fr");
    app.setApplicationName("MediaCasterServer");

    QtWebEngine::initialize();

    QNetworkAccessManager qnam;
    QQmlApplicationEngine engine;

    QObject::connect(&engine, SIGNAL(quit()), &app, SLOT(quit()), Qt::QueuedConnection);

    QQmlContext* context = engine.rootContext();

    context->setContextProperty("appVersion", APPLICATION_VERSION);

    Playlist playlist(qnam);
    PlayerController playerController(playlist);
    PlaylistServer playlistServer(playerController, playlist);

    qmlRegisterUncreatableType<MediaPlatform>("com.taltenbach.mediacaster.mediaplatform", 1, 0, "MediaPlatform",
                                              "Not intended to be created, only used to hold enum.");

    qmlRegisterUncreatableType<PlaylistEnqueueReply>("com.taltenbach.mediacaster.playlistenqueuereply",
                                                   1, 0, "PlaylistEnqueueReply",
                                                   "Not intended to be created from QML");

    qmlRegisterUncreatableType<PlaylistEntryWrapper>("com.taltenbach.mediacaster.playlistentrywrapper",
                                                     1, 0, "PlaylistEntryWrapper",
                                                     "Cannot be created from QML, no default constructor");

    qmlRegisterUncreatableType<MediaStream>("com.taltenbach.mediacaster.mediastream",
                                            1, 0, "MediaStream",
                                            "Not intended to be created from QML");

    context->setContextProperty("playlist", &playlist);
    context->setContextProperty("playerController", &playerController);
    context->setContextProperty("playlistServer", &playlistServer);

    engine.load(QUrl(QLatin1String("qrc:/App.qml")));
    engine.load(QUrl(QLatin1String("qrc:/PlayerWindow.qml")));

    context->setContextProperty("playerWindow", engine.rootObjects()[1]);

    return app.exec();
}
