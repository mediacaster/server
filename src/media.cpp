#include "media.h"

Media::Media() : mID(), mTitle(), mThumbnailURL()
{
    // Empty
}

Media::Media(MediaPlatformID const& id, QString const& title, QString const& thumbnailURL)
    : mID(id), mTitle(title), mThumbnailURL(thumbnailURL)
{
    // Empty
}

Media::Media(QString const& key, MediaPlatform::Platform platform, QString const& title, QString const& thumbnailURL)
    : Media(MediaPlatformID(key, platform), title, thumbnailURL)
{
    // Empty
}

MediaPlatformID const& Media::getID() const
{
    return mID;
}

QString Media::getTitle() const
{
    return mTitle;
}

QString Media::getThumbnailURL() const
{
    return mThumbnailURL;
}

QDataStream& operator<<(QDataStream& out, Media const& media)
{
    out << media.getTitle()
        << media.getID()
        << media.getThumbnailURL();

    return out;
}

QDataStream& operator>>(QDataStream& in, Media& media)
{
    MediaPlatformID id;
    QString title, thumbnailURL;
    in >> title >> id >> thumbnailURL;

    media = Media(id, title, thumbnailURL);

    return in;
}
