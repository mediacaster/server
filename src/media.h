#ifndef MEDIA_H
#define MEDIA_H

#include <QString>
#include <QDataStream>
#include <mediaplatformid.h>
#include <mediaplatform.h>

class Media
{
public:
    Media();
    Media(MediaPlatformID const& id, QString const& title, QString const& thumbnailURL);
    Media(QString const& key, MediaPlatform::Platform platform, QString const& title, QString const& thumbnailURL);
    MediaPlatformID const& getID() const;
    QString getTitle() const;
    QString getThumbnailURL() const;

private:
    MediaPlatformID mID;
    QString mTitle;
    QString mThumbnailURL;
};

QDataStream& operator<<(QDataStream& out, Media const& media);
QDataStream& operator>>(QDataStream& in, Media& media);

#endif // MEDIA_H
