#include "mediainforetrievalreply.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

MediaInfoRetrievalReply::MediaInfoRetrievalReply(QNetworkReply* requestReply, MediaPlatformID const& mediaID)
    : mRequestReply(requestReply), mMediaID(mediaID), mRetrievedInfo(), mRetrievalError(RetrievalError::NoError),
      mNetworkError(QNetworkReply::NetworkError::NoError), mIsFinished(false), mHttpStatusCode(-1)
{
    Q_ASSERT(requestReply != nullptr);
    connect(requestReply, SIGNAL(finished()), this, SLOT(requestFinished()));
}

void MediaInfoRetrievalReply::requestFinished()
{
    mIsFinished = true;

    parseReply();

    mRequestReply->deleteLater();
    mRequestReply = nullptr;

    emit finished(this);
}

void MediaInfoRetrievalReply::parseReply()
{
    QVariant statusCodeVariant = mRequestReply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    mHttpStatusCode = statusCodeVariant.isValid() ? statusCodeVariant.toInt() : -1;

    if (mRequestReply->error() != QNetworkReply::NoError)
    {
        if (mRequestReply->error() == QNetworkReply::ContentNotFoundError)
        {
            mRetrievalError = RetrievalError::InvalidVideoID;
        }
        else
        {
            mRetrievalError = RetrievalError::NetworkError;
            mNetworkError = mRequestReply->error();
        }

        return;
    }

    if (mHttpStatusCode != 200)
    {
        mRetrievalError = RetrievalError::APIError;
        return;
    }

    QByteArray replyData = mRequestReply->readAll();

    switch (mMediaID.getPlatform())
    {
        case MediaPlatform::YOUTUBE:
            mRetrievalError = parseYoutubeReply(replyData);
            break;
        case MediaPlatform::VIMEO:
            mRetrievalError = parseVimeoReply(replyData);
            break;
        default:
            Q_ASSERT(false);
            break;
    }
}

MediaInfoRetrievalReply::RetrievalError MediaInfoRetrievalReply::parseYoutubeReply(QByteArray const& reply)
{
    QJsonArray videos = QJsonDocument::fromJson(reply)
                       .object().value("items")
                       .toArray();

    if (videos.size() == 0)
        return RetrievalError::InvalidVideoID;

    QJsonObject info = videos.at(0)
                      .toObject().value("snippet")
                      .toObject();

    QString title = info.value("title").toString();
    QString thumbnailURL = info.value("thumbnails")
                           .toObject().value("medium")
                           .toObject().value("url")
                           .toString();

    if (title.isNull() || thumbnailURL.isNull())
        return RetrievalError::ReplyParsingError;

    mRetrievedInfo = Media(mMediaID, title, thumbnailURL);

    return RetrievalError::NoError;
}

MediaInfoRetrievalReply::RetrievalError MediaInfoRetrievalReply::parseVimeoReply(QByteArray const& reply)
{
    QJsonObject info = QJsonDocument::fromJson(reply).object();

    QString title = info.value("name").toString();

    QJsonArray thumbnailArray = info.value("pictures")
                                .toObject().value("sizes")
                                .toArray();

    int thumbnailIndex = thumbnailArray.size() < 3 ? 0 : 2;

    QString thumbnailURL = thumbnailArray.at(thumbnailIndex)
                           .toObject().value("link")
                           .toString();

    if (title.isNull() || thumbnailURL.isNull())
        return RetrievalError::ReplyParsingError;

    mRetrievedInfo = Media(mMediaID, title, thumbnailURL);

    return RetrievalError::NoError;
}

Media MediaInfoRetrievalReply::getRetrievedInfo() const
{
    Q_ASSERT(mIsFinished && mRetrievalError == RetrievalError::NoError);
    return mRetrievedInfo;
}

MediaInfoRetrievalReply::RetrievalError MediaInfoRetrievalReply::getRetrievalError() const
{
    return mRetrievalError;
}

QNetworkReply::NetworkError MediaInfoRetrievalReply::getNetworkError() const
{
    return mNetworkError;
}

bool MediaInfoRetrievalReply::isFinished() const
{
    return mIsFinished;
}

int MediaInfoRetrievalReply::getHttpStatusCode() const
{
    return mHttpStatusCode;
}
