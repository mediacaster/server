#ifndef MEDIAINFORETRIEVALREPLY_H
#define MEDIAINFORETRIEVALREPLY_H

#include <QObject>
#include <QNetworkReply>
#include <media.h>

class MediaInfoRetrievalReply : public QObject
{
    Q_OBJECT

public:
    enum class RetrievalError
    {
        NoError = 0,        /* No error */
        NetworkError,       /* Network error, see <getNetworkError> for more details */
        InvalidVideoID,     /* The specified video ID does not exist on the given platform */
        ReplyParsingError,  /* Impossible to parse the reply from the API */
        APIError            /* Other platform API error, see <getHttpStatusCode> for more details */
    };

    MediaInfoRetrievalReply(QNetworkReply* requestReply, MediaPlatformID const& mediaID);

    /**
     * @brief Returns the retrieved information
     *
     * This method can only be called once the request has successfully finished
     * (<isFinished> returns true and <getRetrievalError> returns RetrievalError::NoError)
     */
    Media getRetrievedInfo() const;

    RetrievalError getRetrievalError() const;
    QNetworkReply::NetworkError getNetworkError() const;
    bool isFinished() const;
    int getHttpStatusCode() const;

signals:
    void finished(MediaInfoRetrievalReply* reply);

private slots:
    void requestFinished();

private:
    QNetworkReply* mRequestReply;
    MediaPlatformID mMediaID;
    Media mRetrievedInfo;
    RetrievalError mRetrievalError;
    QNetworkReply::NetworkError mNetworkError;
    bool mIsFinished;
    int mHttpStatusCode;

    void parseReply();
    RetrievalError parseYoutubeReply(QByteArray const& reply);
    RetrievalError parseVimeoReply(QByteArray const& reply);
};

#endif // MEDIAINFORETRIEVALREPLY_H
