#include "mediainforetriever.h"
#include <QUrl>
#include <QUrlQuery>

MediaInfoRetriever::MediaInfoRetriever(QNetworkAccessManager& qnam) : mNetworkAccessManager(qnam)
{
    // Empty
}

MediaInfoRetrievalReply* MediaInfoRetriever::retrieveMediaInfo(MediaPlatformID const& mediaID)
{
    QNetworkRequest videoInfoRequest = MediaInfoRetriever::createRetrievalRequest(mediaID);
    QNetworkReply* networkReply = mNetworkAccessManager.get(videoInfoRequest);

    return new MediaInfoRetrievalReply(networkReply, mediaID);
}

QNetworkRequest MediaInfoRetriever::createRetrievalRequest(MediaPlatformID const& mediaID)
{
    static const QString YOUTUBE_API_KEY = "AIzaSyAxhJGsgJIcN8wT7FwIjHYd_aME-anMlFs";
    static const QString VIMEO_API_KEY = "ZGNjZGU4ZThlYzhhMzJhNjA0YThhMzg4ZjY2ODg2MjNjYjYzZGIzNjp1azVqTk95eVVRdmVaeFZBQktsUFRGZTd6bEtBZDMrRFRLZVV0UEtjdS9Zc0N3TkUxcG5VTkh0SHFYNHpVbkxGZGJ1NlNFcXJzeWQ1U1NZVEFkbFNPSVVSanJQZHh5Vk4wZ01mUU5xM28rMkduUmp5bzNDL0d0N0VMbTFPS1hxVg==";

    switch (mediaID.getPlatform())
    {
        case MediaPlatform::YOUTUBE:
        {
            QUrl url("https://www.googleapis.com/youtube/v3/videos");

            QUrlQuery query;
            query.addQueryItem("key", YOUTUBE_API_KEY);
            query.addQueryItem("id", mediaID.getKey());
            query.addQueryItem("part", "snippet");
            query.addQueryItem("fields", "items(snippet(title, thumbnails(medium)))");

            url.setQuery(query);

            return QNetworkRequest(url);
        }
        case MediaPlatform::VIMEO:
        {
            QUrl url("https://api.vimeo.com/videos/" + mediaID.getKey());

            QUrlQuery query;
            query.addQueryItem("fields", "name,pictures.sizes.link");

            url.setQuery(query);

            QNetworkRequest request(url);
            request.setRawHeader(QByteArray("Authorization"), ("basic " + VIMEO_API_KEY).toUtf8());

            return request;
        }
        default:
            Q_ASSERT(false);
            return QNetworkRequest();
    }
}
