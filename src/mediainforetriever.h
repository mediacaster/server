#ifndef MEDIAINFORETRIEVER_H
#define MEDIAINFORETRIEVER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <media.h>
#include <mediainforetrievalreply.h>

class MediaInfoRetriever
{
public:
    MediaInfoRetriever(QNetworkAccessManager& qnam);

    /**
     * @brief Retrieves asynchronously the information on a media from the platform data API
     * @param mediaID The platform ID of the media
     * @return A pointer to a new RetrievalReply enabling to get the information retrived after the request has finished.
     *
     * Connect to the <finished> signal of the reply returned to be notified when the request is done.
     * Please note that the reply returned is dynamically allocated and must threfore be manually deleted.
     */
    MediaInfoRetrievalReply* retrieveMediaInfo(MediaPlatformID const& mediaID);

private:
    QNetworkAccessManager& mNetworkAccessManager;

    static QNetworkRequest createRetrievalRequest(MediaPlatformID const& mediaID);
};

#endif // MEDIAINFORETRIEVER_H
