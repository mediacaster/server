#ifndef MEDIAPLATFORM_H
#define MEDIAPLATFORM_H

#include <QObject>

struct MediaPlatform {
    Q_GADGET

public:
    enum Platform {
        YOUTUBE = 0,
        VIMEO = 1
        // TODO: Add more
    };
    Q_ENUM(Platform)

    MediaPlatform() = delete;
};

#endif // MEDIAPLATFORM_H
