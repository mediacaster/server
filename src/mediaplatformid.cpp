#include "mediaplatformid.h"
#include <QRegExp>

#define PLATFORM_COUNT 2

MediaPlatformID::MediaPlatformID() : mKey(), mPlatform()
{
    // Empty
}

MediaPlatformID::MediaPlatformID(QString const& key, MediaPlatform::Platform platform) : mKey(key), mPlatform(platform)
{
    // Empty
}

MediaPlatformID::MediaPlatformID(QString const& url)
{
    MediaPlatformID id;

    if (!MediaPlatformID::fromURL(&id, url))
        throw std::invalid_argument("Invalid video url");

    mKey = id.mKey;
    mPlatform = id.mPlatform;
}

QString MediaPlatformID::getKey() const
{
    return mKey;
}

MediaPlatform::Platform MediaPlatformID::getPlatform() const
{
    return mPlatform;
}

QRegExp const* MediaPlatformID::identifyPlatformFromURL(QString const& url, MediaPlatform::Platform* platform)
{
    static const QRegExp platformRegexes[PLATFORM_COUNT] = {
        QRegExp("^(https?://)?((((www.)?|(m.)?)youtube.com)|(youtu.be))/((watch\\?v=)|(v/)|(embed/))?"),
        QRegExp("^(\\s)?(https?://)?vimeo.com/")
    };

    int i = 0;

    while (i < PLATFORM_COUNT && platformRegexes[i].indexIn(url) != 0)
        ++i;

    if (i >= PLATFORM_COUNT)
        return nullptr;

    if (platform != nullptr)
        *platform = static_cast<MediaPlatform::Platform>(i);

    return &platformRegexes[i];
}

bool MediaPlatformID::isValidVideoURL(QString const& url)
{
    return identifyPlatformFromURL(url) != nullptr;
}

bool MediaPlatformID::fromURL(MediaPlatformID* id, QString const& url)
{
    MediaPlatform::Platform platform;
    QRegExp const* matchingRegex = identifyPlatformFromURL(url, &platform);

    if (matchingRegex == nullptr)
        return false;

    QString key;

    switch (platform)
    {
        case MediaPlatform::YOUTUBE:
        {
            int beginKeyInd = matchingRegex->matchedLength();
            int endKeyInd = url.indexOf("&", beginKeyInd);

            key = url.mid(beginKeyInd, (endKeyInd == -1) ? -1 : endKeyInd - beginKeyInd);
            break;
        }
        case MediaPlatform::VIMEO:
        {
            key = url.mid(matchingRegex->matchedLength());
            break;
        }
    }

    *id = MediaPlatformID(key, platform);
    return true;
}

QDataStream& operator<<(QDataStream& out, MediaPlatformID const& id)
{
    out << static_cast<qint16>(id.getPlatform())
        << id.getKey();

    return out;
}

QDataStream& operator>>(QDataStream& in, MediaPlatformID& id)
{
    qint16 platform;
    QString key;
    in >> platform >> key;

    id = MediaPlatformID(key, static_cast<MediaPlatform::Platform>(platform));

    return in;
}
