#ifndef MEDIAPLATFORMID_H
#define MEDIAPLATFORMID_H

#include <QString>
#include <QDataStream>
#include <mediaplatform.h>

/**
 * @brief Uniquely identifies a media on a platform
 */
class MediaPlatformID
{
public:
    MediaPlatformID();
    MediaPlatformID(QString const& key, MediaPlatform::Platform platform);
    MediaPlatformID(QString const& url);
    QString getKey() const;
    MediaPlatform::Platform getPlatform() const;

    static bool isValidVideoURL(QString const& url);
    static bool fromURL(MediaPlatformID* id, QString const& url);

private:
    QString mKey;
    MediaPlatform::Platform mPlatform;

    static QRegExp const* identifyPlatformFromURL(QString const& url, MediaPlatform::Platform* platform = nullptr);
};

QDataStream& operator<<(QDataStream& out, MediaPlatformID const& id);
QDataStream& operator>>(QDataStream& in, MediaPlatformID& id);

#endif // MEDIAPLATFORMID_H
