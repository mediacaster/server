#include "mediastream.h"
#include <QProcess>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QCoreApplication>

#ifdef _WIN32
    #define YOUTUBE_DL_EXE_FILE "youtube-dl.exe"
#else
    #define YOUTUBE_DL_EXE_FILE "youtube-dl"
#endif

MediaStream::MediaStream(QObject* parent) : QObject(parent), mAvailableQualities(), mVideoStreamUrls(), mAudioStreamUrl(), mState(State::RETRIEVING_DATA)
{
    // Empty
}

QString MediaStream::getVideoStreamUrl(QString const& quality) const
{
    return mVideoStreamUrls.value(quality);
}

QString MediaStream::getAudioStreamUrl() const
{
    return mAudioStreamUrl;
}

QStringList const& MediaStream::getAvailableVideoQualities() const
{
    return mAvailableQualities;
}

MediaStream::State MediaStream::getState() const
{
    return mState;
}

void MediaStream::destroy()
{
    deleteLater();
}

MediaStream* MediaStream::fromPlatformMedia(MediaPlatformID const& platformMediaId, QObject* parent)
{
    // Currently only YouTube platform is supported
    if (platformMediaId.getPlatform() != MediaPlatform::Platform::YOUTUBE)
        return nullptr;

    MediaStream* mediaStream = new MediaStream(parent);
    QProcess* process = new QProcess(mediaStream);

    QObject::connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit, process, [process]() {
        process->disconnect();
        process->terminate();
        process->deleteLater();
    });

    QObject::connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), [mediaStream, process](int exitCode, QProcess::ExitStatus exitStatus) {
        process->deleteLater();

        if (exitStatus == QProcess::ExitStatus::CrashExit || exitCode != 0)
        {
            mediaStream->mState = State::RETRIEVING_ERROR;
            emit mediaStream->stateChanged(State::RETRIEVING_ERROR);
            return;
        }

        QJsonArray output = QJsonDocument::fromJson(process->readAllStandardOutput())
                            .object().value("formats")
                            .toArray();

        int audioAbr = -1;
        QString audioUrl;

        for (QJsonArray::const_iterator it = output.constBegin(); it != output.constEnd(); ++it)
        {
            QJsonObject cur = it->toObject();
            QString format = cur.value("format_note").toString();

            if (cur.value("ext").toString() != "webm")
                continue;

            if (cur.value("vcodec").toString() == "none") // Audio only
            {
                int abr = cur.value("abr").toInt();

                if (abr > audioAbr)
                {
                    audioAbr = abr;
                    audioUrl = cur.value("url").toString();
                }
            }
            else if (cur.value("acodec").toString() == "none") // Video only
            {
                mediaStream->mAvailableQualities.append(format);
                mediaStream->mVideoStreamUrls.insert(format, cur.value("url").toString());
            }
        }

        mediaStream->mAudioStreamUrl = audioUrl;

        mediaStream->mState = State::READY;
        emit mediaStream->stateChanged(State::READY);
    });

    QObject::connect(process, &QProcess::errorOccurred, [mediaStream]() {
        mediaStream->mState = State::RETRIEVING_ERROR;
        emit mediaStream->stateChanged(State::RETRIEVING_ERROR);
        return;
    });

    process->start(YOUTUBE_DL_EXE_FILE, QStringList() << "-j" << "--youtube-skip-dash-manifest" << "--id" << platformMediaId.getKey());

    return mediaStream;
}
