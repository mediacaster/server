#ifndef MEDIASTREAM_H
#define MEDIASTREAM_H

#include <QObject>
#include <QString>
#include <QHash>
#include <mediaplatformid.h>

class MediaStream : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList const& videoQualities READ getAvailableVideoQualities NOTIFY stateChanged)
    Q_PROPERTY(QString audioStreamUrl READ getAudioStreamUrl NOTIFY stateChanged)
    Q_PROPERTY(MediaStream::State state READ getState NOTIFY stateChanged)

    public:
        enum class State {
            RETRIEVING_ERROR = -1,
            RETRIEVING_DATA = 0,
            READY = 1
        };
        Q_ENUM(State)

        Q_INVOKABLE QString getVideoStreamUrl(QString const& quality) const;

        QString getAudioStreamUrl() const;

        QStringList const& getAvailableVideoQualities() const;

        State getState() const;

        Q_INVOKABLE void destroy();

        /**
         * @brief Gets asynchronously the media stream associated with the specified platform media
         * @param platformMediaId The platform media ID
         * @param parent The parent of the new media stream that will be created
         * @return The media stream associated with the specified platform media if this function supports the given platform,
         *         nullptr otherwise.
         *
         * The media stream is retrieved asynchronously, that means the data contained in the media stream are empty until the
         * latter emits the stateChanged signal.
         *
         * Please note that this function dynamically create a MediaStream object, and the caller must therefore take care of its
         * desallocation.
         *
         * Currently only YouTube videos are supported.
         */
        static MediaStream* fromPlatformMedia(MediaPlatformID const& platformMediaId, QObject* parent = nullptr);

    signals:
        void stateChanged(MediaStream::State newState);

    private:
        QStringList mAvailableQualities;
        QHash<QString, QString> mVideoStreamUrls;
        QString mAudioStreamUrl;
        State mState;

        MediaStream(QObject* parent = nullptr);
};

Q_DECLARE_METATYPE(MediaStream::State);

#endif // MEDIASTREAM_H
