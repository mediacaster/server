#include "message.h"

void Message::writeToDataStream(QDataStream& out)
{
    Q_UNUSED(out)
    // Empty
}

Message::Header Message::readMessageHeader(QDataStream& in)
{
    qint16 header;
    in >> header;
    return static_cast<Header>(header);
}

Message::Header Message::readMessageHeader(QIODevice* device)
{
    QDataStream in(device);
    return readMessageHeader(in);
}
