#ifndef MESSAGE_H
#define MESSAGE_H

#include <QDataStream>
#include <QIODevice>

/**
 * @brief Contains utility functions and constants that can be used to read and write
 *        messages between a playlist client and server.
 */
class Message
{
public:
    Message() = delete;

    enum class Header
    {
        ERROR_ENQUEUE_FAILED = -2,
        ERROR_CONNECTION_FAILED = -1,
        CLIENT_INFO = 1,
        CONNECTION_ACCEPTED = 2,
        CLIENT_KICKED = 3,
        CLIENT_BANNED = 4,
        SERVER_STOPPED = 5,
        ENTRY_ENQUEUED = 6,
        ENTRY_REMOVED = 7,
        ENTRY_MOVED_UP = 8,
        ENTRY_MOVED_DOWN = 9,
        CURRENT_ENTRY_CHANGED = 10,
        OUT_OF_MEDIA = 11,
        ENQUEUE_MEDIA_URL = 12,
        ENQUEUE_MEDIA_ID = 13,
        REMOVE_ENTRY = 14,
        KICK_USER = 15,
        BAN_USER = 16,
        PLAY_ENTRY = 17,
        VOTE_TO_SKIP_CURRENT = 18,
        SKIP_VOTE_UPDATED = 19,
        PING = 20,
        PONG = 21,
        PLAYER_STATE_CHANGED = 22,
        RESUME_PLAYBACK = 23,
        PAUSE_PLAYBACK = 24,
        SEEK_TO = 25,
        SKIP_NEXT = 26,
        SKIP_PREVIOUS = 27,
        VOLUME_CHANGED = 28,
        CHANGE_VOLUME = 29/*,
        MOVE_VIDEO_UP = 14,
        MOVE_VIDEO_DOWN = 15*/
    };

    /**
     * @brief Writes a message, consisting of a header and a content, in the provided
     *        QDataStream.
     * @param out The QDataStream in which to write the message
     * @param header The header of the message
     * @param content The content of the message
     *
     * The header is firstly written, then all the elements of the content in the order
     * they are provided.
     *
     * Note that operator<< of QDataStream must be implemented for all the types of the
     * elements of the content.
     */
    template<typename... Types>
    static void writeMessage(QDataStream& out, Header header, Types const&... content)
    {
        out << static_cast<qint16>(header);
        writeToDataStream(out, content...);
    }

    template<typename... Types>
    static void writeMessage(QIODevice* device, Header header, Types const&... content)
    {
        QDataStream out(device);
        writeMessage(out, header, content...);
    }

    static Header readMessageHeader(QDataStream& in);

    static Header readMessageHeader(QIODevice* device);

private:
    static void writeToDataStream(QDataStream& out);

    template<typename T, typename... Types>
    static void writeToDataStream(QDataStream& out, T const& value, Types const&... others)
    {
        out << value;
        writeToDataStream(out, others...);
    }
};

#endif // MESSAGE_H
