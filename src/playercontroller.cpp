#include <QCoreApplication>
#include <QThread>
#include "playercontroller.h"

PlayerController::PlayerController(Playlist const& playlist)
    : mPlaylist(playlist), mState(PlayerState::IDLE), mLastPlayingStateTime(0), mLastCurrentMediaTimeUpdate(0), mMediaDuration(0), mCurrentEntryIndex(-1), mVolume(100)
{
    Q_ASSERT(QThread::currentThread() == QCoreApplication::instance()->thread());

    connect(&playlist, SIGNAL(afterMediaEnqueued()), this, SLOT(onAfterMediaEnqueued()));
    connect(&playlist, SIGNAL(entryRemoved(unsigned long, int)), this, SLOT(onEntryRemoved(unsigned long, int)));
    connect(&playlist, SIGNAL(entryMovedUp(unsigned long, int)), this, SLOT(onEntryMovedUp(unsigned long, int)));
    connect(&playlist, SIGNAL(entryMovedDown(unsigned long, int)), this, SLOT(onEntryMovedDown(unsigned long, int)));

    if (mPlaylist.size() > 0)
        playEntry(0);
    else
        mCurrentEntryIndex = 0;
}

void PlayerController::playEntry(int index)
{
    Q_ASSERT(index >= 0 && index < mPlaylist.size());

    setState(PlayerState::LOADING);

    if (index != mCurrentEntryIndex)
    {
        mCurrentEntryIndex = index;
        emit currentEntryChanged(mCurrentEntryIndex, mPlaylist[index]);
    }
    else
    {
        emit soughtTo(0);
    }
}

bool PlayerController::playEntryByID(unsigned long entryID, int startSearchIndex)
{
    int entryInd = mPlaylist.indexOf(entryID, startSearchIndex);

    if (entryInd < 0)
        return false;

    playEntry(entryInd);
    return true;
}

void PlayerController::pause()
{
    if (mState == PlayerState::PAUSED)
        return;

    emit doPause();
}

void PlayerController::resume()
{
    if (mState != PlayerState::PAUSED)
        return;

    emit doResume();
}

void PlayerController::seekTo(unsigned long long time)
{
    if (mState == PlayerState::IDLE)
        return;

    emit soughtTo(time);
}

void PlayerController::skipNext()
{
    if (mCurrentEntryIndex == mPlaylist.size())
        return;

    ++mCurrentEntryIndex;
    playCurrentEntry();
}

void PlayerController::skipPrevious()
{
    if (!hasPreviousEntry())
        return;

    playEntry(mCurrentEntryIndex - 1);
}

void PlayerController::setVolume(unsigned int volume)
{
    if (volume == mVolume)
        return;

    mVolume = volume;
    emit volumeChanged(volume);
}

unsigned int PlayerController::getVolume() const
{
    return mVolume;
}

unsigned long long PlayerController::getMediaDuration() const
{
    return mMediaDuration;
}

PlaylistEntry const* PlayerController::getCurrentEntry() const
{
    if (mCurrentEntryIndex >= mPlaylist.size())
        return nullptr;

    return &mPlaylist[mCurrentEntryIndex];
}

PlaylistEntryWrapper* PlayerController::getCurrentEntryWrapper() const
{
    PlaylistEntry const* currentEntry = getCurrentEntry();
    return currentEntry == nullptr ? nullptr : new PlaylistEntryWrapper(*currentEntry);
}

bool PlayerController::hasCurrentEntry() const
{
    return mCurrentEntryIndex < mPlaylist.size();
}

bool PlayerController::hasNextEntry() const
{
    return mCurrentEntryIndex < mPlaylist.size();
}

bool PlayerController::hasPreviousEntry() const
{
    return mCurrentEntryIndex != 0 && mPlaylist.size() > 0;
}

static unsigned long long getCurrentTime()
{
    using namespace std::chrono;

    time_point<steady_clock> now = steady_clock::now();
    time_point<steady_clock, milliseconds> nowSeconds = time_point_cast<milliseconds>(now);

    return static_cast<unsigned long long>(nowSeconds.time_since_epoch().count());
}

void PlayerController::notifyPlaybackStarted(unsigned long long currentMediaTime, unsigned long long duration)
{
    Q_ASSERT(currentMediaTime <= duration);

    mMediaDuration = duration;
    mLastPlayingStateTime = getCurrentTime();
    setState(PlayerState::PLAYING, currentMediaTime);
}

void PlayerController::notifyPlaybackEnded()
{
    skipNext();
}

void PlayerController::notifyLoading(unsigned long long currentMediaTime)
{
    setState(PlayerState::LOADING, currentMediaTime);
}

void PlayerController::notifyPaused(unsigned long long currentMediaTime)
{
    setState(PlayerState::PAUSED, currentMediaTime);
}

MediaStream* PlayerController::getCurrentMediaStream()
{
    PlaylistEntry const* currentEntry = getCurrentEntry();

    if (currentEntry == nullptr)
        return nullptr;

    return MediaStream::fromPlatformMedia(currentEntry->getMedia().getID(), this);
}

void PlayerController::playCurrentEntry()
{
    if (mCurrentEntryIndex < mPlaylist.size())
    {
        setState(PlayerState::LOADING);
        emit currentEntryChanged(mCurrentEntryIndex, mPlaylist[mCurrentEntryIndex]);
    }
    else
    {
        mMediaDuration = 0;
        setState(PlayerState::IDLE);
        emit outOfMedia();
    }
}

void PlayerController::setState(PlayerState state, unsigned long long currentMediaTime)
{
    if (state == mState && currentMediaTime == mLastCurrentMediaTimeUpdate)
        return;

    mState = state;
    mLastCurrentMediaTimeUpdate = currentMediaTime;
    emit stateChanged(state, currentMediaTime);
}

void PlayerController::onAfterMediaEnqueued()
{
    if (mCurrentEntryIndex == mPlaylist.size() - 1)
        playCurrentEntry();
}

void PlayerController::onEntryRemoved(unsigned long entryID, int index)
{
    Q_UNUSED(entryID);

    if (mCurrentEntryIndex > index)
        --mCurrentEntryIndex;
    else if (mCurrentEntryIndex == index)
        playCurrentEntry();
}

void PlayerController::onEntryMovedUp(unsigned long entryID, int index)
{
    Q_UNUSED(entryID);

    if (mCurrentEntryIndex == index)
        --mCurrentEntryIndex;
    else if (mCurrentEntryIndex == index - 1)
        ++mCurrentEntryIndex;
}

void PlayerController::onEntryMovedDown(unsigned long entryID, int index)
{
    Q_UNUSED(entryID);

    if (mCurrentEntryIndex == index)
        ++mCurrentEntryIndex;
    else if (mCurrentEntryIndex == index + 1)
        --mCurrentEntryIndex;
}

QDataStream& operator<<(QDataStream& out, PlayerController const& playerController)
{
    quint64 currentMediaTime = playerController.mLastCurrentMediaTimeUpdate;

    if (playerController.mState == PlayerController::PlayerState::PLAYING)
        currentMediaTime += (getCurrentTime() - playerController.mLastPlayingStateTime);

    out << static_cast<qint32>(playerController.hasCurrentEntry() ? playerController.mCurrentEntryIndex : -1)
        << static_cast<qint16>(playerController.mState)
        << currentMediaTime
        << static_cast<quint64>(playerController.mMediaDuration)
        << static_cast<qint32>(playerController.mVolume);

    return out;
}
