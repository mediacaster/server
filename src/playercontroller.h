#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include <QObject>
#include <playlist.h>
#include <mediastream.h>

/**
 * @brief Used to control the playback of the entries of a playlist
 *
 * This class is intended to be used in the main Qt thread and all methods calls *must* be performed in that thread.
 */
class PlayerController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int volume READ getVolume NOTIFY volumeChanged)

    public:
        enum class PlayerState {
            IDLE = 0,
            LOADING = 1,
            PLAYING = 2,
            PAUSED = 3
        };

        PlayerController(Playlist const& playlist);

        /**
          * @brief Plays the specified entry
          * @param index The index of the entry to play
          */
        Q_INVOKABLE void playEntry(int index);

        /**
         * @brief Plays the specified entry
         * @param entryID ID of the entry to remove
         * @param startSearchInd The index in the playlist where to start the search (used has a hint to speed up the search)
         * @return true if a entry with the specified ID has been found and played, false otherwise
         */
        bool playEntryByID(unsigned long entryID, int startSearchIndex = 0);

        /**
         * @brief Pauses the playback if the player is playing
         */
        void pause();

        /**
         * @brief Resumes the playback if it was paused
         */
        void resume();

        /**
         * @brief Seeks to a specified time in the current media.
         * @param time Time in ms
         *
         * If the player is paused when this function is called, the playback
         * will be resumed.
         */
        void seekTo(unsigned long long time);

        /**
         * @brief Starts the playing of the next entry in the playlist
         *
         * If the playlist is empty or the playlist have no current entry this method has no effect.
         * If the current entry is the last playlist entry, this method stops the playing of the current
         * entry and emits outOfMedia signal.
         */
        void skipNext();

        /**
         * @brief Starts the playing of the previous entry in the playlist.
         *
         * If the playlist has no previous entry this method has no effect.
         *
         * If there is no current entry (ie. the player is IDLE), this method
         * plays the last entry of the playlist if the latter is not empty.
         */
        void skipPrevious();

        /**
         * @brief Sets the player volume.
         * @param volume The volume in percentage of the the maximum volume.
         */
        Q_INVOKABLE void setVolume(unsigned int volume);

        /**
         * @brief Indicates the current player volume.
         * @return The current player volume in percentage of the maximum volume.
         */
        unsigned int getVolume() const;

        /**
         * @brief Gets the duration of the currently played media.
         * @return The duration of the currently played media (in ms)
         *
         * If there is not current entry (ie. the player is IDLE), this method
         * returns 0.
         */
        unsigned long long getMediaDuration() const;

        /**
         * @brief Returns a pointer towards the entry currently played entry if there is one, a nullptr otherwise
         * @return The currently played entry if there is one, nullptr otherwise
         */
        PlaylistEntry const* getCurrentEntry() const;

        /**
          * @brief Returns a QObject wrapper of the entry currently played if there is one, a nullptr otherwise
          *
          * IMPORTANT: The wrapper is dynamically allocated and must therefore be manually destroyed by the caller.
          */
        Q_INVOKABLE PlaylistEntryWrapper* getCurrentEntryWrapper() const;

        /**
          * @brief Indicates whether or not there is an entry currently played in the playlist
          * @return true if the player is currently playing an entry, false otherwise
          */
        Q_INVOKABLE bool hasCurrentEntry() const;

        /**
         * @brief Indicates whether there is still entry to play after the current one.
         * @return true if there is an entry after the current one, false otherwise.
         */
        bool hasNextEntry() const;

        /**
         * @brief Indicates whether there is an entry before the current one.
         * @return true if there is an entry before the current one, false otherwise.
         *
         * If there is no current entry (ie. the player is IDLE), this method returns true
         * iff the playlist is not empty.
         */
        bool hasPreviousEntry() const;

        /**
          * @brief Notifies the player controller the the playback of the playback has started.
          * @param currentMediaTime The current media time (in ms)
          * @param mediaDuration The media duration (in ms)
          *
          * This method is only intended to be called by the player when the playback of a new entry
          * is about to start and when the playback is restarting after having been paused or stopped
          * for buffering purposes.
          */
        Q_INVOKABLE void notifyPlaybackStarted(unsigned long long currentMediaTime, unsigned long long mediaDuration);

        /**
          * @brief Notifies the player controller that the playback of the current entry has ended.
          *
          * This method is only intended to be called by the player at the end of the playback.
          */
        Q_INVOKABLE void notifyPlaybackEnded();

        /**
          * @brief Notifies the player controller that the player is currently in loading state.
          * @param currentMediaTime The current media time (in ms)
          *
          * This method is only intended to be called by the player when the player loads a new media and when
          * the playback has been stopped because the player buffer is not filled enough to continue the playback.
          */
        Q_INVOKABLE void notifyLoading(unsigned long long currentMediaTime);

        /**
          * @brief Notifies the player controller that the playback has been paused.
          * @param currentMediaTime The current media time (in ms)
          *
          * This method is only intended to be called by the player when the playback of
          * the current entry has been paused.
          */
        Q_INVOKABLE void notifyPaused(unsigned long long currentMediaTime);

        /**
          * @brief Gets the media stream associated with the current playing media.
          * @return The media stream associated with the current playing media if there is one and the stream
          *         can be retrieved, a nullptr otherwise.
          *
          * This method is mainly intended to be called by the player.
          */
        Q_INVOKABLE MediaStream* getCurrentMediaStream();

    signals:
        /**
         * @brief Indicates that the playlist has a new current entry
         * @param newIndex Index of the new current entry when the change occured
         * @param video The new current entry
         */
        void currentEntryChanged(int newIndex, PlaylistEntry const& newCurrentEntry);

        /**
          * @brief Indicates that the last media of the playlist has been played
          */
        void outOfMedia();

        /**
         * @brief Indicates that the player state has changed.
         * @param newState The new player state
         * @param currentMediaTime The current media time (in ms)
         *
         * If the player has no current media (ie. player is IDLE), the current media time
         * is zero.
         */
        void stateChanged(PlayerController::PlayerState newState, unsigned long long currentMediaTime);

        /**
         * @brief Indicates that the player has sought to the specified time in the current media.
         * @param time The time in ms
         */
        void soughtTo(unsigned long long time);

        /**
         * @brief Indicates that the player volume has been changed.
         * @param volume The new player volume in percentage of the maximum volume.
         */
        void volumeChanged(unsigned int volume);

        /**
         * @brief Orders the player to resume the playback
         *
         * This signal is only intended to be listened by the player itself.
         */
        void doResume();

        /**
         * @brief Orders the player to pause the playback
         *
         * This signal is only intended to be listened by the player itself.
         */
        void doPause();

    private slots:
        void onAfterMediaEnqueued();

        void onEntryRemoved(unsigned long entryID, int index);

        void onEntryMovedUp(unsigned long entryID, int index);

        void onEntryMovedDown(unsigned long entryID, int index);

    private:
        Playlist const& mPlaylist;                       /**< The played playlist */
        PlayerState mState;                              /**< The state of the player */
        unsigned long long mLastPlayingStateTime;        /**< Last time the player state has changed to PLAYING */
        unsigned long long mLastCurrentMediaTimeUpdate;  /**< Last update of the current media time (in ms) */
        unsigned long long mMediaDuration;               /**< The duration of the currently playing entry (in ms) */
        int mCurrentEntryIndex;                          /**< The index of the currently played entry */
        unsigned int mVolume;                            /**< The volume of the player (in percentage of the maximum volume) */

        void playCurrentEntry();

        void setState(PlayerState state, unsigned long long currentMediaTime = 0);

        friend QDataStream& operator<<(QDataStream& out, PlayerController const& playerController);
};

#endif // PLAYERCONTROLLER_H
