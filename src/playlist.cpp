#include <QCoreApplication>
#include <QThread>
#include "playlist.h"
#include "playlistentry.h"

Playlist::Playlist(QNetworkAccessManager& qnam) : mMediaInfoRetriever(qnam), mEntryModel()
{
    Q_ASSERT(QThread::currentThread() == QCoreApplication::instance()->thread());
    // Empty
}

void Playlist::enqueueMedia(Media const& info, QString const& adderName) {
    emit mediaEnqueued(mEntryModel.enqueueMedia(info, adderName));
    emit afterMediaEnqueued();
}

PlaylistEnqueueReply* Playlist::enqueueMedia(MediaPlatformID const& id, QString const& adderName)
{
    if (mEntryModel.isFull())
        return nullptr;

    PlaylistEnqueueReply* enqueueReply = new PlaylistEnqueueReply(id, this);
    MediaInfoRetrievalReply* retrievalReply = mMediaInfoRetriever.retrieveMediaInfo(id);

    connect(retrievalReply, &MediaInfoRetrievalReply::finished, this,
            [this, enqueueReply, adderName](MediaInfoRetrievalReply* retrievalReply)
            {
                switch (retrievalReply->getRetrievalError())
                {
                    case MediaInfoRetrievalReply::RetrievalError::NoError:
                        enqueueMedia(retrievalReply->getRetrievedInfo(), adderName);
                        emit enqueueReply->success(enqueueReply);
                        break;
                    case MediaInfoRetrievalReply::RetrievalError::NetworkError:
                        emit enqueueReply->networkError(enqueueReply, retrievalReply->getNetworkError());
                        break;
                    case MediaInfoRetrievalReply::RetrievalError::InvalidVideoID:
                        emit enqueueReply->invalidPlatformID(enqueueReply);
                        break;
                    case MediaInfoRetrievalReply::RetrievalError::APIError:
                    case MediaInfoRetrievalReply::RetrievalError::ReplyParsingError:
                        emit enqueueReply->platformAPIError(enqueueReply, retrievalReply->getHttpStatusCode());
                        break;
                }

                enqueueReply->setParent(nullptr);
                retrievalReply->deleteLater();        
            });

    return enqueueReply;
}

PlaylistEnqueueReply* Playlist::enqueueMedia(QString const& url, QString const& adderName)
{
    MediaPlatformID id;

    if (!MediaPlatformID::fromURL(&id, url))
        return nullptr;

    return enqueueMedia(id, adderName);
}

void Playlist::removeEntry(int index)
{
    Q_ASSERT(index >= 0 && index < mEntryModel.size());

    unsigned long entryID = mEntryModel[index].getID();

    mEntryModel.removeEntry(index);

    emit entryRemoved(entryID, index);
}

bool Playlist::removeEntryByID(unsigned long entryID, int startSearchInd)
{
    int entryInd = mEntryModel.getEntryIndex(entryID, startSearchInd);

    if (entryInd < 0)
        return false;

    removeEntry(entryInd);
    return true;
}

void Playlist::postRemoveEntry(unsigned long entryID, int startSearchInd)
{
    postToMainThread([=]() {
        removeEntryByID(entryID, startSearchInd);
    });
}

void Playlist::moveEntryUp(int index)
{
    Q_ASSERT(index >= 0 && index < mEntryModel.size());

    if (index == 0)
        return;

    unsigned long entryID = mEntryModel[index].getID();

    mEntryModel.moveEntryUp(index);

    emit entryMovedUp(entryID, index);
}

bool Playlist::moveEntryUpByID(unsigned long entryID, int startSearchInd)
{
    int entryInd = mEntryModel.getEntryIndex(entryID, startSearchInd);

    if (entryInd < 0)
        return false;

    moveEntryUp(entryInd);
    return true;
}

void Playlist::postMoveEntryUp(unsigned long entryID, int startSearchInd)
{
    postToMainThread([=]() {
        moveEntryUpByID(entryID, startSearchInd);
    });
}

void Playlist::moveEntryDown(int index)
{
    Q_ASSERT(index >= 0 && index < mEntryModel.size());

    if (index == mEntryModel.size() - 1)
        return;

    unsigned long entryID = mEntryModel[index].getID();

    mEntryModel.moveEntryDown(index);

    emit entryMovedDown(entryID, index);
}

bool Playlist::moveEntryDownByID(unsigned long entryID, int startSearchInd)
{
    int entryInd = mEntryModel.getEntryIndex(entryID, startSearchInd);

    if (entryInd < 0)
        return false;

    moveEntryDown(entryInd);
    return true;
}

void Playlist::postMoveEntryDown(unsigned long entryID, int startSearchInd)
{
    postToMainThread([=]() {
        moveEntryDownByID(entryID, startSearchInd);
    });
}

int Playlist::indexOf(unsigned long entryID, int startSearchInd) const
{
    return mEntryModel.getEntryIndex(entryID, startSearchInd);
}

bool Playlist::isEmpty() const
{
    return mEntryModel.isEmpty();
}

int Playlist::size() const
{
    return mEntryModel.size();
}

QAbstractListModel* Playlist::getEntryModel()
{
    return &mEntryModel;
}

template<typename F>
void Playlist::postToMainThread(F&& func)
{
    QObject src;
    connect(&src, &QObject::destroyed, this, std::forward<F>(func));
}

PlaylistEntry const& Playlist::operator[](int index) const
{
    return mEntryModel[index];
}

QDataStream& operator<<(QDataStream& out, Playlist const& playlist)
{
    PlaylistEntryModel const& model = playlist.mEntryModel;
    int count = model.size();

    out << static_cast<qint32>(count);

    for (int i = 0; i < count; ++i)
        out << model[i];

    return out;
}
