#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QObject>
#include <QList>
#include <QNetworkAccessManager>
#include <QAbstractListModel>
#include <playlistentry.h>
#include <playlistentrywrapper.h>
#include <mediainforetriever.h>
#include <playlistenqueuereply.h>
#include <playlistentrymodel.h>

/**
 * @brief Represents a playlist of medias
 *
 * This class is intended to be used in the main Qt thread and all methods calls *must* be performed in that thread
 * except for these which explicitely specify "@theadsafe" in their documentation. Those thread safe functions
 * are non-blocking and performed their task asynchronously.
 */
class Playlist : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QAbstractListModel* entryModel READ getEntryModel CONSTANT)

public:
    Playlist(QNetworkAccessManager& qnam);

    /**
     * @threadsafe
     * @brief Enqueues asynchronously a new media in the playlist
     * @param id The ID of the media to add
     * @param adderName The name of the user who is trying to add the media
     * @return A pointer to a PlaylistmediaReply enabling to know whether or not the enque has succeeded,
     *         if there is still free playlist IDs a nullptr otherwise.
     *
     * Connect to the signals of the reply returned to be notified whether or not the enque has succeeded.
     * Please note that the reply returned is dynamically allocated and must therefore be manually deleted.
     */
    PlaylistEnqueueReply* enqueueMedia(MediaPlatformID const& id, QString const& adderName);

    /**
      * @threadsafe
      * @brief Enqueues asynchronously a new media in the playlist
      * @param url The URL of the media to add
      * @param The name of the user who is trying to add the media
      * @return A pointer to a PlaylistmediaReply enabling to know whether or not the enque has succeeded
      *         if the URL is valid and supported and there is still free playlist IDs, a nullptr otherwise.
      *
      * Connect to the signals of the reply returned to be notified whether or not the enque has succeeded.
     ,* Please note that the reply returned is dynamically allocated and must therefore be manually deleted.
      */
    Q_INVOKABLE PlaylistEnqueueReply* enqueueMedia(QString const& url, QString const& adderName);

    /**
      * @brief Removes an entry from the playlist by its index
      * @param index Index of the entry to remove
      * @warning This function is not thread safe and must only be called from UI thread
      */
    Q_INVOKABLE void removeEntry(int index);

    /**
     * @brief Removes an entry from the playlist by its ID
     * @param entryID ID of the entry to remove
     * @param startSearchInd The index in the list of entries where to start the search
     * @return true if a entry with the specified ID has been found and removed, false otherwise
     * @warning This function is not thread safe and must only be called from UI thread
     */
    bool removeEntryByID(unsigned long entryID, int startSearchInd = 0);

    /**
     * @brief Removes an entry from the playlist by its ID if the corresponding entry
     *        matches the specified predicate
     * @param entryID ID of the entry to remove
     * @param predicate The unary predicate, taking a const reference to a PlaylistEntry as sole argument, used to
     *                  decide whether or not to remove the entry
     * @param startSearchInd The index in the list of entries where to start the search
     * @return true if an entry with the specified ID has been found and removed, false otherwise
     * @warning This function is not thread safe and must only be called from UI thread
     */
    template<typename UnaryPredicate>
    bool removeEntryByIDIf(unsigned long entryID, UnaryPredicate predicate, int startSearchInd = 0)
    {
        int entryInd = mEntryModel.getEntryIndex(entryID, startSearchInd);

        if (entryInd < 0 || !predicate(mEntryModel[entryInd]))
            return false;

        removeEntry(entryInd);
        return true;
    }

    /**
     * @threadsafe
     * @brief Removes asynchronously an entry from the playlist
     * @param entryID The ID of the entry to remove
     * @param startSearchInd The index in the list of entries where to start the search
     * @return true if an entry having the specified ID has been found and removed,
     *         false otherwise
     *
     * The search start from the specified index and continues on both sides of the index until
     * finding the entry having the given ID or having traversed the whole entry list.
     * That start index can thus be set at the current entry index in the entry list to speed up
     * the search.
     */
    void postRemoveEntry(unsigned long entryID, int startSearchInd = 0);

    /**
     * @brief Moves an entry up in the playlist
     */
    Q_INVOKABLE void moveEntryUp(int index);
    bool moveEntryUpByID(unsigned long entryID, int startSearchInd = 0);
    void postMoveEntryUp(unsigned long entryID, int startSearchInd = 0);

    /**
     * @brief Moves an entry down in the playlist
     */
    Q_INVOKABLE void moveEntryDown(int index);
    bool moveEntryDownByID(unsigned long entryID, int startSearchInd = 0);
    void postMoveEntryDown(unsigned long entryID, int startSearchInd = 0);

    /**
     * @brief Indicates the index of the entry having the specified entry ID
     * @return The index of the entry having the specified entry ID if the latter exists,
     *         a stricly negative integer otherwise
     */
    int indexOf(unsigned long entryID, int startSearchInd = 0) const;

    /**
     * @brief Indicates the number of entries currently in the playlist.
     * @return The number of entries in the playlist.
     */
    int size() const;

    /**
     * @brief Indicates whether the playlist is empty or not.
     */
    bool isEmpty() const;

    /**
     * @brief Returns the model managing the entries of the playlist
     */
    QAbstractListModel* getEntryModel();

    /**
     * @brief Gets the entry at the specified index
     * @param index The index of entry
     * @return The entry at the specified index
     */
    PlaylistEntry const& operator[](int index) const;

signals:
    /**
     * @brief Indicates that a new media has been enqueued in the playlist
     * @param newMedia The new media
     */
    void mediaEnqueued(PlaylistEntry const& newEntry);

    /**
     * @brief Fired just after a media has been enqueued and the mediaEnqueued signal has been emitted
     */
    void afterMediaEnqueued();

    /**
     * @brief Indicates that an entry has been removed from the playlist
     * @param entryID The ID of the removed entry
     * @param index The index of the entry when it was removed
     */
    void entryRemoved(unsigned long entryID, int index);

    /**
     * @brief Indicates that an entry in the playlist has been moved one index up
     * @param entryID The ID of the moved entry
     * @param oldIndex The index of the entry before it was moved
     */
    void entryMovedUp(unsigned long entryID, int oldIndex);

    /**
     * @brief Indicates that an entry in the playlist has been moved one index down
     * @param entryID The ID of the moved entry
     * @param oldIndex The index of the entry before it was moved
     */
    void entryMovedDown(unsigned long entryID, int oldIndex);

private:
    MediaInfoRetriever mMediaInfoRetriever;     /**< Used to retrieve the information on the medias from the platform data API */
    PlaylistEntryModel mEntryModel;             /**< The model managing the entries of the playlist */

    void enqueueMedia(Media const& info, QString const& adderName);

    template<typename F>
    void postToMainThread(F&& func);

    friend QDataStream& operator<<(QDataStream& out, Playlist const& playlist);
};

#endif // PLAYLIST_H
