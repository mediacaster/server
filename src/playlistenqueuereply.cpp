#include "playlistenqueuereply.h"

PlaylistEnqueueReply::PlaylistEnqueueReply(MediaPlatformID const& platformID, QObject* parent) : QObject(parent), mPlatformID(platformID)
{
    // Empty
}

QString PlaylistEnqueueReply::getPlatformKey() const
{
    return mPlatformID.getKey();
}

MediaPlatform::Platform PlaylistEnqueueReply::getPlatform() const
{
    return mPlatformID.getPlatform();
}
