#ifndef PLAYLISTENQUEUEREPLY_H
#define PLAYLISTENQUEUEREPLY_H

#include <QObject>
#include <QNetworkReply>
#include <mediaplatformid.h>
#include <mediaplatform.h>

class PlaylistEnqueueReply : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString platformKey READ getPlatformKey CONSTANT)
    Q_PROPERTY(MediaPlatform::Platform platform READ getPlatform CONSTANT)

public:
    /**
     * @param toEnqueID ID of the video to enque
     */
    PlaylistEnqueueReply(MediaPlatformID const& platformID, QObject* parent = nullptr);

    QString getPlatformKey() const;
    MediaPlatform::Platform getPlatform() const;

signals:
    /**
     * @brief Indicates that the media was successfully enqueued to the playlist
     * @param reply Pointer to the reply corresponding to the enqueue
     */
    void success(PlaylistEnqueueReply* reply);

    /**
     * @brief Indicates that the media could not be enqueued to the playlist because the ID is invalid
     * @param reply Pointer to the reply corresponding to the enqueue
     */
    void invalidPlatformID(PlaylistEnqueueReply* reply);

    /**
     * @brief Indicates that a media could not be enqueued to the playlist because of the specified network error
     * @param error The network error which has occured
     * @param reply Pointer to the reply corresponding to the enque
     */
    void networkError(PlaylistEnqueueReply* reply, QNetworkReply::NetworkError error);

    /**
     * @brief Indicates that a media could not be enqueued to the playlist because of an invalid API reply
     * @param httpStatusCode The HTTP status code that the API has replied
     * @param reply Pointer to the reply corresponding to the enqueue
     */
    void platformAPIError(PlaylistEnqueueReply* reply, int httpStatusCode);

private:
    MediaPlatformID mPlatformID;
};

#endif // PLAYLISTENQUEUEREPLY_H
