#include "playlistentry.h"
#include <QtGlobal>

PlaylistEntry::PlaylistEntry() : mID(), mMedia(), mAdderName()
{
    // Empty
}

PlaylistEntry::PlaylistEntry(unsigned long id, Media const& media, QString const& adderName)
    : mID(id), mMedia(media), mAdderName(adderName)
{
    // Empty
}

unsigned long PlaylistEntry::getID() const
{
    return mID;
}

Media const& PlaylistEntry::getMedia() const
{
    return mMedia;
}

QString PlaylistEntry::getAdderName() const
{
    return mAdderName;
}

QDataStream& operator<<(QDataStream& out, PlaylistEntry const& entry)
{
    // Media location (platform) and media type (video)
    // Currently hard-coded but will be dynamically determined
    // when support to audio and local media will be added
    // TODO: add support to audio and local media
    out << static_cast<qint16>(1) << static_cast<qint16>(1);

    out << entry.getMedia()
        << static_cast<quint64>(entry.getID())
        << entry.getAdderName();

    return out;
}

QDataStream& operator>>(QDataStream& in, PlaylistEntry& entry)
{
    // Ignore media location and media type
    // Will be used when support to audio and local media will
    // be added
    // TODO: add support to audio and local media
    qint16 ignore;
    in >> ignore >> ignore;

    Media media;
    quint64 entryID;
    QString adderName;
    in >> media >> entryID >> adderName;

    entry = PlaylistEntry(entryID, media, adderName);

    return in;
}
