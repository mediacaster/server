#ifndef PLAYLISTENTRY_H
#define PLAYLISTENTRY_H

#include <QString>
#include <QDataStream>
#include <media.h>

/**
 * @brief Represents a video of a playlist
 */
class PlaylistEntry
{
public:
    PlaylistEntry();
    PlaylistEntry(unsigned long id, Media const& media, QString const& adderName);

    unsigned long getID() const;
    Media const& getMedia() const;
    QString getAdderName() const;

private:
    unsigned long mID;      /**< Uniquely identifies an entry in a playlist */
    Media mMedia;            /**< The media contained in this playlist entry */
    QString mAdderName;          /**< The name of the person who has added the media in the playlist */
};

QDataStream& operator<<(QDataStream& out, PlaylistEntry const& entry);
QDataStream& operator>>(QDataStream& in, PlaylistEntry& entry);

Q_DECLARE_METATYPE(PlaylistEntry)

#endif // PLAYLISTENTRY_H
