#include "playlistentrymodel.h"

PlaylistEntryModel::PlaylistEntryModel(QObject* parent) : QAbstractListModel(parent), mEntries(), mNextEntryID(0)
{
    // Empty
}

int PlaylistEntryModel::getEntryIndex(unsigned int entryID, int startSearchInd) const
{
    int i;

    if (mEntries.isEmpty())
        return -1;

    if (startSearchInd < 0)
        i = 0;
    else if (startSearchInd >= mEntries.size())
        i = mEntries.size() - 1;
    else
        i = startSearchInd;

    if (mEntries[i].getID() == entryID)
        return i;

    int j = i;

    do
    {
        --i;
        ++j;
    } while (i >= 0 && j < mEntries.size() && mEntries[i].getID() != entryID
             && mEntries[j].getID() != entryID);

    if (i >= 0 && mEntries[i].getID() == entryID)
		return i; // found
	
    if (j < mEntries.size() && mEntries[j].getID() == entryID)
		return j; // found
			 
	// not found yet : at least one bound has been reached before finding the entry
    if (i >= 0)
    {
        do
        {
            --i;
        } while (i >= 0 && mEntries[i].getID() != entryID);

        return i; // if not found, i == -1
    }
    else if (j < mEntries.size())
    {
        do
        {
            ++j;
        } while (j < mEntries.size() && mEntries[j].getID() != entryID);

        return j < mEntries.size() ? j : -1;
    }

    return -1;
}

PlaylistEntry const& PlaylistEntryModel::enqueueMedia(Media const& media, QString const& adderName)
{
    if (isFull())
        throw std::overflow_error("No more video can be added, the whole playlist ID range has been used.");

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    mEntries.append(PlaylistEntry(mNextEntryID++, media, adderName));
    endInsertRows();

    return mEntries.last();
}

void PlaylistEntryModel::removeEntry(int index)
{
    Q_ASSERT(index >= 0 && index < mEntries.size());

    beginRemoveRows(QModelIndex(), index, index);
    mEntries.removeAt(index);
    endRemoveRows();
}

void PlaylistEntryModel::moveEntryUp(int index)
{
    Q_ASSERT(index >= 0 && index < mEntries.size());

    if (index == 0)
        return;

    beginMoveRows(QModelIndex(), index, index, QModelIndex(), index - 1);
    mEntries.swap(index, index - 1);
    endMoveRows();
}

void PlaylistEntryModel::moveEntryDown(int index)
{
    Q_ASSERT(index >= 0 && index < mEntries.size());

    if (index == mEntries.size() - 1)
        return;

    beginMoveRows(QModelIndex(), index, index, QModelIndex(), index + 2);
    mEntries.swap(index, index + 1);
    endMoveRows();
}

bool PlaylistEntryModel::isEmpty() const
{
    return mEntries.isEmpty();
}

bool PlaylistEntryModel::isFull() const
{
    return mNextEntryID == UINT_MAX;
}

int PlaylistEntryModel::size() const
{
    return mEntries.size();
}

int PlaylistEntryModel::rowCount(QModelIndex const& parent) const
{
    Q_UNUSED(parent);
    return mEntries.size();
}

QVariant PlaylistEntryModel::data(QModelIndex const& index, int role) const
{
    if (index.row() < 0 || index.row() >= mEntries.size())
        return QVariant();

    PlaylistEntry const& entry = mEntries[index.row()];

    switch (static_cast<Roles>(role))
    {
        case Roles::TitleRole:
            return entry.getMedia().getTitle();
        case Roles::ThumbnailURLRole:
            return entry.getMedia().getThumbnailURL();
        case Roles::AdderNameRole:
            return entry.getAdderName();
    }

    return QVariant();
}

QHash<int, QByteArray> PlaylistEntryModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>(Roles::TitleRole)] = "title";
    roles[static_cast<int>(Roles::ThumbnailURLRole)] = "thumbnailURL";
    roles[static_cast<int>(Roles::AdderNameRole)] = "adderName";

    return roles;
}

PlaylistEntry const& PlaylistEntryModel::operator[](int index) const
{
    return mEntries[index];
}
