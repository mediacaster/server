#ifndef PLAYLISTENTRYMODEL_H
#define PLAYLISTENTRYMODEL_H

#include <QAbstractListModel>
#include <playlistentry.h>

// TODO: make thread safe

class PlaylistEntryModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum class Roles {
        TitleRole = Qt::UserRole + 1,
        ThumbnailURLRole,
        AdderNameRole
    };

    PlaylistEntryModel(QObject* parent = nullptr);

    /**
     * @brief Enqueues a media at the end of the entry list
     * @param video The information on the media to enque
     * @param adderName The name of the user who is enqueueing the media
     * @return Reference to newly enqueued media
     * @throw std::overflow_error if no more entry ID can be generated
     */
    PlaylistEntry const& enqueueMedia(Media const& media, QString const& adderName);

    /**
     * @brief Returns the index of the entry having the specified ID in the entry list
     * @param The ID of the entry to search for
     * @param The index in the list of entries where to start the search
     * @return The index of the entry if the specified entry ID exists in the entry list (positive or zero integer),
     *         a strictly negative integer otherwise
     *
     * The search start from the specified index and continues on both sides of the index until
     * finding the entry having the given ID or having traversed the whole entry list.
     */
    int getEntryIndex(unsigned int entryID, int startSearchInd = 0) const;

    /**
     * @brief Removes an entry from the playlist
     * @param index The index of the entry
     */
    void removeEntry(int index);

    /**
     * @brief Moves an entry up in the entry list
     * @param index The index of the entry
     */
    void moveEntryUp(int index);

    /**
     * @brief Moves an entry down in the entry list
     * @param index The index of the entry
     */
    void moveEntryDown(int index);

    /**
     * @brief Indicates if the entry list is empty
     * @return true if the entry list is empty, false otherwise
     */
    bool isEmpty() const;

    /**
     * @brief Indicates whether or not there is still free entry IDs, enabling to enqueue more medias
     * @return true if no more medias can be added, false otherwise
     */
    bool isFull() const;

    int size() const;

    int rowCount(QModelIndex const& parent = QModelIndex()) const override;

    QVariant data(QModelIndex const& index, int role = Qt::DisplayRole) const override;

    PlaylistEntry const& operator[](int index) const;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QList<PlaylistEntry> mEntries;               /**< The entries managed by the model */
    unsigned long mNextEntryID;                  /**< The ID of the next entry which will be added to the playlist */
};

#endif // PLAYLISTENTRYMODEL_H
