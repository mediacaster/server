#include "playlistentrywrapper.h"

PlaylistEntryWrapper::PlaylistEntryWrapper(PlaylistEntry const& entry) : mEntry(entry)
{
    // Empty
}

unsigned long PlaylistEntryWrapper::getEntryID() const
{
    return mEntry.getID();
}

QString PlaylistEntryWrapper::getPlatformKey() const
{
    return mEntry.getMedia().getID().getKey();
}

MediaPlatform::Platform PlaylistEntryWrapper::getPlatform() const
{
    return mEntry.getMedia().getID().getPlatform();
}

QString PlaylistEntryWrapper::getTitle() const
{
    return mEntry.getMedia().getTitle();
}

QString PlaylistEntryWrapper::getThumbnailURL() const
{
    return mEntry.getMedia().getThumbnailURL();
}

QString PlaylistEntryWrapper::getAdderName() const
{
    return mEntry.getAdderName();
}

void PlaylistEntryWrapper::destroyWrapper()
{
    deleteLater();
}
