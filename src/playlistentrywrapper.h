#ifndef PLAYLISTENTRYWRAPPER_H
#define PLAYLISTENTRYWRAPPER_H

#include <QObject>
#include <playlistentry.h>
#include <mediaplatform.h>

/**
 * @brief Wraps a PlaylistVideo into a QObject enabling sharing with QML
 */
class PlaylistEntryWrapper : public QObject
{
    Q_OBJECT

    Q_PROPERTY(unsigned long entryID READ getEntryID CONSTANT)
    Q_PROPERTY(QString platformKey READ getPlatformKey CONSTANT)
    Q_PROPERTY(MediaPlatform::Platform platform READ getPlatform CONSTANT)
    Q_PROPERTY(QString adderName READ getAdderName CONSTANT)
    Q_PROPERTY(QString title READ getTitle CONSTANT)
    Q_PROPERTY(QString thumbnailURL READ getThumbnailURL CONSTANT)

public:
    PlaylistEntryWrapper(PlaylistEntry const& entry);

    unsigned long getEntryID() const;
    QString getPlatformKey() const;
    MediaPlatform::Platform getPlatform() const;
    QString getTitle() const;
    QString getThumbnailURL() const;
    QString getAdderName() const;

    Q_INVOKABLE void destroyWrapper();

private:
    PlaylistEntry const mEntry;
};

#endif // PLAYLISTENTRYWRAPPER_H
