#include <QNetworkInterface>
#include <QThread>
#include <QCoreApplication>
#include "playlistserver.h"
#include "clientconnectiontask.h"
#include "message.h"

const QDataStream::Version PlaylistServer::QDATASTREAM_VERSION = QDataStream::Qt_5_11;
const QDataStream::ByteOrder PlaylistServer::QDATASTREAM_BYTE_ORDER = QDataStream::BigEndian;

PlaylistServer::PlaylistServer(PlayerController& playerController, Playlist& playlist)
    : mServer(), mPlayerController(playerController), mPlaylist(playlist), mConnectedUsers(), mBannedUserIDs(),
      mAdminPasswordHash(), mSkipCurrentEntryVote(nullptr), mIsRunning(false)
{
    Q_ASSERT(QThread::currentThread() == QCoreApplication::instance()->thread());

    connect(&mServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    connect(&mPlayerController, SIGNAL(currentEntryChanged(int, PlaylistEntry const&)), this, SLOT(onCurrentEntryChanged(int, PlaylistEntry const&)));
    connect(&mPlayerController, SIGNAL(outOfMedia()), this, SLOT(onOutOfMedia()));
    connect(&mPlayerController, SIGNAL(stateChanged(PlayerController::PlayerState, unsigned long long)), this, SLOT(onPlayerStateChanged(PlayerController::PlayerState, unsigned long long)));
    connect(&mPlayerController, SIGNAL(soughtTo(unsigned long long)), this, SLOT(onPlayerSoughtTo(unsigned long long)));
    connect(&mPlayerController, SIGNAL(volumeChanged(unsigned int)), this, SLOT(onPlayerVolumeChanged(unsigned int)));

    connect(&mPlaylist, SIGNAL(mediaEnqueued(PlaylistEntry const&)), this, SLOT(onMediaEnqueued(PlaylistEntry const&)));
    connect(&mPlaylist, SIGNAL(entryRemoved(unsigned long, int)), this, SLOT(onEntryRemoved(unsigned long, int)));
    connect(&mPlaylist, SIGNAL(entryMovedUp(unsigned long, int)), this, SLOT(onEntryMovedUp(unsigned long, int)));
    connect(&mPlaylist, SIGNAL(entryMovedDown(unsigned long, int)), this, SLOT(onEntryMovedDown(unsigned long, int)));
}

void PlaylistServer::startServer(quint16 port, QString const& adminPassword, float skipVoteFavorableThreshold)
{
    if (mIsRunning)
        return;

    mAdminPasswordHash = hashPassword(adminPassword.toUtf8());

    if (skipVoteFavorableThreshold >= 0.0f)
        mSkipCurrentEntryVote = new Vote(skipVoteFavorableThreshold, this);
    else
        mSkipCurrentEntryVote = nullptr; // Disable vote

    connect(mSkipCurrentEntryVote, SIGNAL(voteUpdated(int, int)), this, SLOT(onSkipCurrentEntryVoteUpdated(int, int)));
    connect(mSkipCurrentEntryVote, SIGNAL(votePassed()), this, SLOT(onSkipCurrentEntryVotePassed()));

    mServer.listen(QHostAddress::Any, port);
    mIsRunning = true;

    emit serverStateChanged(true);
}

void PlaylistServer::stopServer()
{
    if (!mIsRunning)
        return;

    mServer.close();
    mIsRunning = false;

    QHashIterator<QString, PlaylistUser*> it(mConnectedUsers);

    while (it.hasNext())
        it.next().value()->kick(Message::Header::SERVER_STOPPED);

    delete mSkipCurrentEntryVote;
    mSkipCurrentEntryVote = nullptr;

    emit serverStateChanged(false);
}

bool PlaylistServer::isRunning() const
{
    return mIsRunning;
}

void PlaylistServer::kickUser(QString const& userName)
{
    PlaylistUser* user = getUserByName(userName);

    if (user != nullptr)
        user->kick();
}

void PlaylistServer::banUser(QString const& userName)
{
    PlaylistUser* user = getUserByName(userName);

    if (user != nullptr)
        banUser(user);
}

void PlaylistServer::unbanUser(QString const& userID)
{
    if (mBannedUserIDs.remove(userID))
        emit userUnbanned(userID);
}

QString PlaylistServer::getServerStringAddress()
{
    QList<QHostAddress> ipAddresses = QNetworkInterface::allAddresses();

    int i = 0;

    while (i < ipAddresses.size() &&
           (ipAddresses[i] == QHostAddress::LocalHost || ipAddresses[i].protocol() != QAbstractSocket::IPv4Protocol))
    {
        ++i;
    }

    if (i == ipAddresses.size())
        return QHostAddress(QHostAddress::LocalHost).toString();
    else
        return ipAddresses[i].toString();
}

void PlaylistServer::setUpDataStream(QDataStream& dataStream)
{
    dataStream.setVersion(QDATASTREAM_VERSION);
    dataStream.setByteOrder(QDATASTREAM_BYTE_ORDER);
}

void PlaylistServer::onNewConnection()
{
    QTcpSocket* newClientSocket = mServer.nextPendingConnection();
    connect(newClientSocket, SIGNAL(disconnected()), newClientSocket, SLOT(deleteLater()));

    ClientConnectionTask* connectionTask = new ClientConnectionTask(*newClientSocket, this);

    connect(connectionTask, SIGNAL(connectionSucceeded(QTcpSocket*, ClientConnectionInfo const&)),
            this, SLOT(onUserConnectionSucceeded(QTcpSocket*, ClientConnectionInfo const&)));

    connect(connectionTask, SIGNAL(connectionFailed(QTcpSocket*, ClientConnectionTask::ConnectionError)),
            this, SLOT(onUserConnectionFailed(QTcpSocket*, ClientConnectionTask::ConnectionError)));

    connectionTask->tryConnect();
}

void PlaylistServer::onUserConnectionSucceeded(QTcpSocket* socket, ClientConnectionInfo const& clientInfo)
{
    ClientConnectionTask* connectionTask = qobject_cast<ClientConnectionTask*>(QObject::sender());
    connectionTask->deleteLater();

    tryConnect(socket, clientInfo);
}

void PlaylistServer::onUserConnectionFailed(QTcpSocket* socket, ClientConnectionTask::ConnectionError error)
{
    ClientConnectionTask* connectionTask = qobject_cast<ClientConnectionTask*>(QObject::sender());
    connectionTask->deleteLater();

    if (error == ClientConnectionTask::ConnectionError::IncompatibleProtocolVersion)
        Message::writeMessage(socket, Message::Header::ERROR_CONNECTION_FAILED, static_cast<qint16>(ConnectionDecision::Reason::INCOMPATIBLE_PROTOCOL));

    socket->flush();
    socket->close();
    socket->deleteLater();
}

void PlaylistServer::onUserDisconnected()
{
    PlaylistUser* user = static_cast<PlaylistUser*>(QObject::sender());
    QString userName = user->getName();

    mConnectedUsers.remove(userName);

    if (mSkipCurrentEntryVote != nullptr && mSkipCurrentEntryVote->isOpen())
        mSkipCurrentEntryVote->removeVoter(userName);

    emit userDisconnected(user->getID(), userName);

    user->deleteLater();
}

void PlaylistServer::onCurrentEntryChanged(int newIndex, PlaylistEntry const& newCurrentEntry)
{
    sendToAll(Message::Header::CURRENT_ENTRY_CHANGED, static_cast<qint64>(newCurrentEntry.getID()), static_cast<qint32>(newIndex));

    if (mSkipCurrentEntryVote != nullptr)
        mSkipCurrentEntryVote->closeVote();
}

void PlaylistServer::onOutOfMedia()
{
    sendToAll(Message::Header::OUT_OF_MEDIA);

    if (mSkipCurrentEntryVote != nullptr)
        mSkipCurrentEntryVote->closeVote();
}

void PlaylistServer::onPlayerStateChanged(PlayerController::PlayerState newState, unsigned long long currentMediaTime)
{
    sendToAdmins(Message::Header::PLAYER_STATE_CHANGED, static_cast<qint16>(newState),
                 static_cast<quint64>(currentMediaTime), static_cast<quint64>(mPlayerController.getMediaDuration()));
}

void PlaylistServer::onPlayerSoughtTo(unsigned long long time)
{
    sendToAdmins(Message::Header::PLAYER_STATE_CHANGED, static_cast<qint16>(PlayerController::PlayerState::PLAYING),
                 static_cast<quint64>(time), static_cast<quint64>(mPlayerController.getMediaDuration()));
}

void PlaylistServer::onPlayerVolumeChanged(unsigned int newVolume)
{
    sendToAdmins(Message::Header::VOLUME_CHANGED, newVolume);
}

void PlaylistServer::onMediaEnqueued(PlaylistEntry const& newEntry)
{
    sendToAll(Message::Header::ENTRY_ENQUEUED, newEntry);
}

void PlaylistServer::onEntryRemoved(unsigned long entryID, int index)
{
    sendToAll(Message::Header::ENTRY_REMOVED, static_cast<qint64>(entryID), static_cast<qint32>(index));
}

void PlaylistServer::onEntryMovedUp(unsigned long entryID, int oldIndex)
{
    sendToAll(Message::Header::ENTRY_MOVED_UP, static_cast<qint64>(entryID), static_cast<qint32>(oldIndex));
}

void PlaylistServer::onEntryMovedDown(unsigned long entryID, int oldIndex)
{
    sendToAll(Message::Header::ENTRY_MOVED_DOWN, static_cast<qint64>(entryID), static_cast<qint32>(oldIndex));
}

void PlaylistServer::onKickRequested(QString const& userName)
{
    PlaylistUser* user = getUserByName(userName);

    if (user != nullptr && !user->isAdmin())
        user->kick();
}

void PlaylistServer::onBanRequested(QString const& userName)
{
    PlaylistUser* user = getUserByName(userName);

    if (user != nullptr && !user->isAdmin())
        banUser(user);
}

void PlaylistServer::onUserVoteToSkipCurrentEntry(QString const& userName, unsigned long currentEntryID)
{
    if (mSkipCurrentEntryVote == nullptr)
        return; // Skip vote is disabled

    if (currentEntryID != mPlayerController.getCurrentEntry()->getID())
        return;

    if (!mSkipCurrentEntryVote->isOpen())
        mSkipCurrentEntryVote->openVote(mConnectedUsers.size());

    mSkipCurrentEntryVote->voteInFavor(userName);
}

void PlaylistServer::onSkipCurrentEntryVoteUpdated(int favorableVotes, int neededFavorableVotes)
{
    if (favorableVotes == 0 || favorableVotes >= neededFavorableVotes)
        return;

    sendToAll(Message::Header::SKIP_VOTE_UPDATED, static_cast<qint32>(favorableVotes), static_cast<qint32>(neededFavorableVotes));
}

void PlaylistServer::onSkipCurrentEntryVotePassed()
{
    mSkipCurrentEntryVote->closeVote();
    mPlayerController.skipNext();
}

void PlaylistServer::tryConnect(QTcpSocket* socket, ClientConnectionInfo const& clientInfo)
{
    ConnectionDecision decision = examineUserConnection(clientInfo);

    switch (decision.mResult)
    {
        case ConnectionDecision::Result::ACCEPTED:
            break;
        case ConnectionDecision::Result::PENDING:
            connectIfDuplicateUserInactive(socket, clientInfo, decision.mDuplicateUser);
            return;
        case ConnectionDecision::Result::REJECTED:
            rejectConnection(socket, decision.mReason);
            return;
    }

    bool isAdmin = hashPassword(clientInfo.getAdminPassword()) == mAdminPasswordHash;

    PlaylistUser* newUser = new PlaylistUser(*socket, mPlayerController, mPlaylist, clientInfo.getUserID(), clientInfo.getUserName(), isAdmin, this);
    mConnectedUsers.insert(clientInfo.getUserName(), newUser);

    bool isSkipVoteEnabled = mSkipCurrentEntryVote != nullptr;

    if (isSkipVoteEnabled && mSkipCurrentEntryVote->isOpen())
        mSkipCurrentEntryVote->addVoter();

    connect(newUser, SIGNAL(disconnected()), this, SLOT(onUserDisconnected()));
    connect(newUser, SIGNAL(requestUserKick(QString const&)), this, SLOT(onKickRequested(QString const&)));
    connect(newUser, SIGNAL(requestUserBan(QString const&)), this, SLOT(onBanRequested(QString const&)));
    connect(newUser, SIGNAL(userVoteToSkipCurrentEntry(QString const&, unsigned long)), this, SLOT(onUserVoteToSkipCurrentEntry(QString const&, unsigned long)));

    newUser->sendMessage(Message::Header::CONNECTION_ACCEPTED, isAdmin, isSkipVoteEnabled, mPlayerController, mPlaylist);

    emit userConnected(newUser->getID(), newUser->getName());
}

QByteArray PlaylistServer::hashPassword(QString const& password) const {
    return QCryptographicHash::hash(password.toUtf8(), QCryptographicHash::Keccak_256);
}

/*bool PlaylistServer::checkIDUniqueness(QString const& userID) const
{
    bool uniqueID = true;

    QMapIterator<QString, PlaylistUser*> it(mConnectedUsers);

    while (it.hasNext() && uniqueID)
        uniqueID = it.next().value()->getID() != userID;

    return uniqueID;
}*/

PlaylistServer::ConnectionDecision PlaylistServer::examineUserConnection(ClientConnectionInfo const& clientInfo) const
{
    if (mBannedUserIDs.contains(clientInfo.getUserID()))
        return ConnectionDecision(ConnectionDecision::Result::REJECTED, ConnectionDecision::Reason::USER_BANNED);

    PlaylistUser* duplicateUser = getUserByName(clientInfo.getUserName());

    if (duplicateUser != nullptr)
    {
        if (duplicateUser->getID() == clientInfo.getUserID())
            return ConnectionDecision(ConnectionDecision::Result::PENDING, ConnectionDecision::Reason::DUPLICATE_USER_ID, duplicateUser);
        else
            return ConnectionDecision(ConnectionDecision::Result::REJECTED, ConnectionDecision::Reason::DUPLICATE_USER_NAME, duplicateUser);
    }

    duplicateUser = getUserByID(clientInfo.getUserID());

    if (duplicateUser!= nullptr)
        return ConnectionDecision(ConnectionDecision::Result::PENDING, ConnectionDecision::Reason::DUPLICATE_USER_ID, duplicateUser);

    return ConnectionDecision(ConnectionDecision::Result::ACCEPTED, ConnectionDecision::Reason::OK);
}

/*bool PlaylistServer::allowUserConnection(QTcpSocket* socket, ClientConnectionInfo const& userInfo)
{
    ConnectionFailedReason reason;

    if (mBannedUserIDs.contains(userInfo.getUserID()))
    {
        Message::writeMessage(socket, Message::Header::ERROR_CONNECTION_FAILED, static_cast<qint16>(ConnectionFailedReason::USER_BANNED));
        return false;
    }

    PlaylistUser* duplicateUser = getUserByName(userInfo.getUserName());

    if (duplicateUser != nullptr)
    {
        if (duplicateUser->getID() != userInfo.getUserID())
            Message::writeMessage(socket, Message::Header::ERROR_CONNECTION_FAILED, static_cast<qint16>(ConnectionFailedReason::DUPLICATE_USER_NAME));
        else
            connectIfDuplicateUserInactive(socket, userInfo, duplicateUser);

        return false;
    }

    duplicateUser = getUserByID(userInfo.getUserID());

    if (duplicateUser != nullptr)
    {
        connectIfDuplicateUserInactive(socket, userInfo, duplicateUser);
        return false;
    }

    return true;
}*/

void PlaylistServer::connectIfDuplicateUserInactive(QTcpSocket* socket, ClientConnectionInfo const& userInfo, PlaylistUser* duplicateUser)
{
    CheckAliveReply* reply = duplicateUser->checkAlive();
    reply->setParent(this);

    connect(reply, &CheckAliveReply::alive, this, [this, socket, reply]() {
        rejectConnection(socket, ConnectionDecision::Reason::DUPLICATE_USER_ID);
        reply->deleteLater();
    });

    QString userName = duplicateUser->getName();

    connect(reply, &CheckAliveReply::dead, this, [this, reply, socket, userInfo, userName]() {
        PlaylistUser* duplicateUser = getUserByName(userName);

        if (duplicateUser != nullptr) // Need to check: user could have been disconnected
            duplicateUser->kick();

        tryConnect(socket, userInfo);
        reply->deleteLater();
    });
}

void PlaylistServer::rejectConnection(QTcpSocket* socket, ConnectionDecision::Reason reason) const
{
    Message::writeMessage(socket, Message::Header::ERROR_CONNECTION_FAILED, static_cast<qint16>(reason));

    socket->flush();
    socket->close();
    socket->deleteLater();
}

PlaylistUser* PlaylistServer::getUserByName(QString const& userName) const
{
    QHash<QString, PlaylistUser*>::const_iterator user = mConnectedUsers.find(userName);
    return user == mConnectedUsers.end() ? nullptr : user.value();
}

PlaylistUser* PlaylistServer::getUserByID(QString const& userID) const
{
    QHashIterator<QString, PlaylistUser*> it(mConnectedUsers);
    bool found = false;

    while (it.hasNext())
        found = it.next().value()->getID() == userID;

    return found ? it.value() : nullptr;
}

template<typename... Types>
void PlaylistServer::sendToAll(Message::Header msgHeader, Types... msgContent)
{
    QHashIterator<QString, PlaylistUser*> it(mConnectedUsers);

    while (it.hasNext())
        it.next().value()->sendMessage(msgHeader, msgContent...);
}

template<typename... Types>
void PlaylistServer::sendToAdmins(Message::Header msgHeader, Types... msgContent)
{
    QHashIterator<QString, PlaylistUser*> it(mConnectedUsers);

    while (it.hasNext()) {
        PlaylistUser* user = it.next().value();

        if (user->isAdmin())
            user->sendMessage(msgHeader, msgContent...);
    }
}

void PlaylistServer::banUser(PlaylistUser* user)
{
    QString userID = user->getID();

    mBannedUserIDs.insert(userID);
    user->kick(Message::Header::CLIENT_BANNED);

    emit userBanned(userID, user->getName());
}
