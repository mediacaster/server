#ifndef PLAYLISTSERVER_H
#define PLAYLISTSERVER_H

#include <QTcpServer>
#include <playlist.h>
#include <playercontroller.h>
#include <playlistuser.h>
#include <clientconnectiontask.h>
#include <vote.h>

// TODO : use mConnectedUser model with a list to optimize storage ?

/**
 * @brief Server enabling to multiple remote clients to interract with the playlist of videos
 *
 * This class is intended to be used in the main Qt thread and all methods calls must be performed in that thread
 * except for these which explicitely specify "@theadsafe" in their documentation. Those thread safe functions
 * are non-blocking and performed their task asynchronously.
 */
class PlaylistServer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString serverAddress READ getServerStringAddress CONSTANT)
    Q_PROPERTY(bool isServerRunning READ isRunning NOTIFY serverStateChanged)

public:
    static const QDataStream::Version QDATASTREAM_VERSION;
    static const QDataStream::ByteOrder QDATASTREAM_BYTE_ORDER;

    PlaylistServer(PlayerController& playerController, Playlist& playlist);

    /**
      * @brief Starts the server
      * @param port The server port
      * @param adminPassword The administrator password
      * @param skipVoteFavorableThreshold Minimum ratio of favorable votes over the number
      *        of voters to skip the currently playing media the vote (inclusive). Set to a
      *        negative value to disable.
      */
    Q_INVOKABLE void startServer(quint16 port, QString const& adminPassword, float skipVoteFavorableThreshold);

    Q_INVOKABLE void stopServer();

    Q_INVOKABLE void kickUser(QString const& userName);

    Q_INVOKABLE void banUser(QString const& userName);

    Q_INVOKABLE void unbanUser(QString const& userName);

    bool isRunning() const;

    static QString getServerStringAddress();

    static void setUpDataStream(QDataStream& dataStream);

signals:
    void userConnected(QString const& userID, QString const& userName);

    void userDisconnected(QString const& userID, QString const& userName);

    void userBanned(QString const& userID, QString const& userName);

    void userUnbanned(QString const& userID);

    void serverStateChanged(bool isRunning);

private slots:
    void onNewConnection();
    void onUserConnectionSucceeded(QTcpSocket* socket, ClientConnectionInfo const& clientInfo);
    void onUserConnectionFailed(QTcpSocket* socket, ClientConnectionTask::ConnectionError error);
    void onUserDisconnected();

    void onCurrentEntryChanged(int newIndex, PlaylistEntry const& newCurrentEntry);
    void onOutOfMedia();
    void onPlayerStateChanged(PlayerController::PlayerState newState, unsigned long long currentMediaTime);
    void onPlayerSoughtTo(unsigned long long time);
    void onPlayerVolumeChanged(unsigned int newVolume);

    void onMediaEnqueued(PlaylistEntry const& newEntry);
    void onEntryRemoved(unsigned long entryID, int index);
    void onEntryMovedUp(unsigned long entryID, int oldIndex);
    void onEntryMovedDown(unsigned long entryID, int oldIndex);

    void onKickRequested(QString const& userName);
    void onBanRequested(QString const& userName);
    void onUserVoteToSkipCurrentEntry(QString const& userName, unsigned long currentEntryID);

    void onSkipCurrentEntryVoteUpdated(int favorableVotes, int neededFavorableVotes);
    void onSkipCurrentEntryVotePassed();

private:
    QTcpServer mServer;
    PlayerController& mPlayerController;
    Playlist& mPlaylist;
    QHash<QString, PlaylistUser*> mConnectedUsers;
    QSet<QString> mBannedUserIDs;
    QByteArray mAdminPasswordHash;
    Vote* mSkipCurrentEntryVote;
    bool mIsRunning;

    struct ConnectionDecision {
        enum class Result {
            ACCEPTED,
            PENDING,
            REJECTED
        };

        enum class Reason {
            OK = 0,
            INCOMPATIBLE_PROTOCOL = 1,
            USER_BANNED = 2,
            DUPLICATE_USER_NAME = 3,
            DUPLICATE_USER_ID = 4
        };

        ConnectionDecision(Result result, Reason reason, PlaylistUser* duplicateUser = nullptr)
            : mResult(result), mReason(reason), mDuplicateUser(duplicateUser)
        {
            // Empty
        }

        Result mResult;
        Reason mReason;
        PlaylistUser* mDuplicateUser;
    };

    /**
     * @brief Tries to connect the client to the server
     * @param socket The client socket
     * @param clientInfo The client info
     *
     * The client info are examined to check if the user is permitted to connect to the server, as defined
     * by {@link #examineUserConnection()}.
     * If the connection is rejected, a message explaining the reason is sent to the client before closing
     * the socket.
     * If the connection is put on hold because of a duplicate user ID, this method will try to check if
     * the currently connection to the user having the same ID is still active and try again to connect if
     * he is not.
     */
    void tryConnect(QTcpSocket* socket, ClientConnectionInfo const& clientInfo);

    /**
     * @brief Hashes the specified password
     * @param password The password to hash
     * @return The hash of the password
     */
    QByteArray hashPassword(QString const& password) const;

    /**
     * @brief Checks if a user is permitted to connect to the server
     * @param connectionInfo The user's connection information
     * @return The decision taken on the connection. The decision result is ACCEPTED if the user is permitted to connect to the server,
     *         PENDING if further asynchronous checks must be performed before taking a final decision, REJECTED if the user is not
     *         permitted to connect.
     *
     * A user is permitted to connect iff its ID is not banned and the server does not contain another user with the same name or ID.
     *
     * If an user with the same ID is already connected to the server, this method returns a decision with PENDING result.
     * That avoids rejecting the reconnection of a user in the case he would have lost its internet connection (leaving the
     * socket connection open on the server side). The caller must then check if the connected user active is still active.
     */
    ConnectionDecision examineUserConnection(ClientConnectionInfo const& userInfo) const;

    /**
     * @brief Tries to connect the new client to the server if the specified user is inactive
     * @param socket The socket connection of the new client
     * @param clientInfo The connection info of the new client
     * @param duplicateUser The potentially inactive user having the same user ID as the new client
     *
     * This method checks asynchronously if the socket connection to the specified duplicate user is still active.
     * If it is, the new connection will be rejected after sending a message to notify the client, and the socket will
     * be closed.
     * If it is not, the inactive socket will be closed and this method will try again to connect the client with the new socket.
     */
    void connectIfDuplicateUserInactive(QTcpSocket* socket, ClientConnectionInfo const& clientInfo, PlaylistUser* duplicateUser);

    /**
     * @brief Rejects a client connection to the server
     * @param socket The client socket
     * @param reason The rejection reason
     *
     * This method sends a message explaining the rejection to the client before closing
     * the socket.
     */
    void rejectConnection(QTcpSocket* socket, ConnectionDecision::Reason reason) const;

    /**
     * @brief Gets the connected user associated with the specified name if it exists
     * @param userName The name of the user
     * @return The connected user associated with the specified name if it exists, nullptr otherwise
     */
    PlaylistUser* getUserByName(QString const& userName) const;

    /**
     * @brief Gets the connected user associated with the specified ID if it exists
     * @param userID The ID of the user
     * @return The connected user associated with the specified ID if it exists, nullptr otherwise
     */
    PlaylistUser* getUserByID(QString const& userID) const;

    /**
     * @brief Sends a message to all the users connected to the server
     * @param msgHeader Header of the message
     * @param msgContent Content of the message
     */
    template<typename... Types>
    void sendToAll(Message::Header msgHeader, Types... msgContent);

    /**
     * @brief Sends a message to all the users connected to the server having the administrators rights
     * @param msgHeader Header of the message
     * @param msgContent Content of the message
     */
    template<typename... Types>
    void sendToAdmins(Message::Header msgHeader, Types... msgContent);

    void banUser(PlaylistUser* user);
};

#endif // PLAYLISTSERVER_H
