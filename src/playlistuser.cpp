#include "playlistuser.h"
#include "playlistserver.h"

PlaylistUser::PlaylistUser(QTcpSocket& socket, PlayerController& playerController, Playlist& playlist, QString id, QString name, bool isAdmin, QObject* parent)
    : QObject(parent), mSocket(socket), mSocketStream(&socket), mPlayerController(playerController), mPlaylist(playlist), mID(id), mName(name), mIsAdmin(isAdmin)
{
    connect(&mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(&mSocket, SIGNAL(disconnected()), this, SIGNAL(disconnected()));

    PlaylistServer::setUpDataStream(mSocketStream);
}

void PlaylistUser::kick(Message::Header kickMsg)
{
    sendMessage(kickMsg);
    mSocket.flush();
    mSocket.close();
}

QString PlaylistUser::getID() const
{
    return mID;
}

QString PlaylistUser::getName() const
{
    return mName;
}

bool PlaylistUser::isAdmin() const
{
    return mIsAdmin;
}

CheckAliveReply* PlaylistUser::checkAlive()
{
    // Do not set this as parent because if the socket disconnect during the check,
    // the PlaylistUser will be destroyed by the server and thus CheckAliveReply too.
    // That way CheckAliveReply::alive would never be called.
    return new CheckAliveReply(mSocket, mSocketStream);
}

void PlaylistUser::onReadyRead()
{
    mSocketStream.startTransaction();

    Message::Header msgHeader = Message::readMessageHeader(mSocketStream);

    if (mSocketStream.status() != QDataStream::Ok)
    {
        mSocketStream.rollbackTransaction();
        return;
    }

    switch (msgHeader)
    {
        case Message::Header::ENQUEUE_MEDIA_URL:
            enqueueMediaURL();
            break;
        case Message::Header::ENQUEUE_MEDIA_ID:
            enqueueMediaID();
            break;
        case Message::Header::REMOVE_ENTRY:
            removeEntry();
            break;
        case Message::Header::KICK_USER:
            kickUser();
            break;
        case Message::Header::BAN_USER:
            banUser();
            break;
        case Message::Header::PLAY_ENTRY:
            playEntry();
            break;
        case Message::Header::VOTE_TO_SKIP_CURRENT:
            voteToSkipCurrent();
            break;
        case Message::Header::PING:
            sendMessage(Message::Header::PONG);
            mSocketStream.commitTransaction();
            break;
        case Message::Header::PONG:
            break;
        case Message::Header::RESUME_PLAYBACK:
            resumePlayback();
            break;
        case Message::Header::PAUSE_PLAYBACK:
            pausePlayback();
            break;
        case Message::Header::SEEK_TO:
            seekTo();
            break;
        case Message::Header::SKIP_NEXT:
            skipNext();
            break;
        case Message::Header::SKIP_PREVIOUS:
            skipPrevious();
            break;
        case Message::Header::CHANGE_VOLUME:
            changeVolume();
            break;
        /*case Message::Header::MOVE_VIDEO_UP:
            moveVideoUp();
            break;
        case Message::Header::MOVE_VIDEO_DOWN:
            moveVideoDown();
            break;*/
        default:
            qWarning() << "No implementation for message: " << static_cast<int>(msgHeader);
            mSocketStream.abortTransaction();
    }
}

void PlaylistUser::enqueueMediaURL()
{
    QString mediaURL;
    mSocketStream >> mediaURL;

    if (!mSocketStream.commitTransaction())
        return;

    PlaylistEnqueueReply* enqueueReply = mPlaylist.enqueueMedia(mediaURL, mName);

    if (enqueueReply == nullptr)
        sendMessage(Message::Header::ERROR_ENQUEUE_FAILED, tr("The specified media URL is invalid."));
    else
        connectToEnqueueReply(enqueueReply);
}

void PlaylistUser::enqueueMediaID()
{
    MediaPlatformID mediaID;
    mSocketStream >> mediaID;

    if (!mSocketStream.commitTransaction())
        return;

    connectToEnqueueReply(mPlaylist.enqueueMedia(mediaID, mName));
}

void PlaylistUser::connectToEnqueueReply(PlaylistEnqueueReply* enqueueReply)
{
    connect(enqueueReply, &PlaylistEnqueueReply::success, this,
            [](PlaylistEnqueueReply* enqueueReply)
            {
                enqueueReply->deleteLater();
            });

    connect(enqueueReply, &PlaylistEnqueueReply::invalidPlatformID, this,
            [this](PlaylistEnqueueReply* enqueueReply)
            {
                sendMessage(Message::Header::ERROR_ENQUEUE_FAILED,
                            tr("The specified video ID does not exist on the given video platform."));

                enqueueReply->deleteLater();
            });

    connect(enqueueReply, &PlaylistEnqueueReply::networkError, this,
            [this](PlaylistEnqueueReply* enqueueReply, QNetworkReply::NetworkError error)
            {
                sendMessage(Message::Header::ERROR_ENQUEUE_FAILED,
                            tr("A network error has occured. Error code: %1.").arg(error));

                enqueueReply->deleteLater();
            });

    connect(enqueueReply, &PlaylistEnqueueReply::platformAPIError, this,
            [this](PlaylistEnqueueReply* enqueueReply, int httpStatusCode)
            {
                sendMessage(Message::Header::ERROR_ENQUEUE_FAILED,
                            tr("The video platform data API has replied incorrectly. Reply code: %1").arg(httpStatusCode));

                enqueueReply->deleteLater();
            });
}

void PlaylistUser::removeEntry()
{
    PlaylistEntryReference entryRef(mSocketStream);

    if (!mSocketStream.commitTransaction())
        return;

    mPlaylist.removeEntryByIDIf(entryRef.mEntryID,
                                [this](PlaylistEntry const& video)
                                {
                                    return mIsAdmin || video.getAdderName() == mName;
                                }, entryRef.mEntryIndex);
}

void PlaylistUser::kickUser()
{
    QString userName;
    mSocketStream >> userName;

    if (!mSocketStream.commitTransaction() || !mIsAdmin)
        return;

    emit requestUserKick(userName);
}

void PlaylistUser::banUser() {
    QString userName;
    mSocketStream >> userName;

    if (!mSocketStream.commitTransaction() || !mIsAdmin)
        return;

    emit requestUserBan(userName);
}

void PlaylistUser::playEntry()
{
    PlaylistEntryReference entryRef(mSocketStream);

    if (!mSocketStream.commitTransaction() || !mIsAdmin)
        return;

    mPlayerController.playEntryByID(entryRef.mEntryID, entryRef.mEntryIndex);
}

void PlaylistUser::voteToSkipCurrent()
{
    quint64 currentEntryID;
    mSocketStream >> currentEntryID;

    if (!mSocketStream.commitTransaction())
        return;

    emit userVoteToSkipCurrentEntry(mName, currentEntryID);
}

void PlaylistUser::resumePlayback()
{
    mSocketStream.commitTransaction();

    if (!mIsAdmin)
        return;

    mPlayerController.resume();
}

void PlaylistUser::pausePlayback()
{
    mSocketStream.commitTransaction();

    if (!mIsAdmin)
        return;

    mPlayerController.pause();
}

void PlaylistUser::seekTo()
{
    quint64 currentEntryID;
    quint32 seconds;
    mSocketStream >> currentEntryID >> seconds;

    if (!mSocketStream.commitTransaction() || !mIsAdmin || !checkCurrentEntry(currentEntryID))
        return;

    mPlayerController.seekTo(seconds * 1000);
}

void PlaylistUser::skipNext()
{
    quint64 currentEntryID;
    mSocketStream >> currentEntryID;

    if (!mSocketStream.commitTransaction() || !mIsAdmin || !checkCurrentEntry(currentEntryID))
        return;

    mPlayerController.skipNext();
}

void PlaylistUser::skipPrevious()
{
    quint64 currentEntryID;
    mSocketStream >> currentEntryID;

    if (!mSocketStream.commitTransaction() || !mIsAdmin || !checkCurrentEntry(currentEntryID))
        return;

    mPlayerController.skipPrevious();
}

void PlaylistUser::changeVolume()
{
    qint32 volume;
    mSocketStream >> volume;

    if (!mSocketStream.commitTransaction() || !mIsAdmin)
        return;

    mPlayerController.setVolume(volume);
}

bool PlaylistUser::checkCurrentEntry(quint64 currentEntryID)
{
    PlaylistEntry const* currentEntry = mPlayerController.getCurrentEntry();

    return (currentEntry == nullptr && static_cast<qint64>(currentEntryID) == -1)
            || (currentEntry != nullptr && currentEntry->getID() == currentEntryID);
}

/*void PlaylistUser::moveEntryUp()
{
    quint64 videoPlaylistID;
    qint32 videoIndex;
    mSocketStream >> videoPlaylistID >> videoIndex;

    if (!mSocketStream.commitTransaction())
        return;

    if (!mIsAdmin)
        return;

    mPlaylist.moveVideoUpByID(videoPlaylistID, videoIndex);
}

void PlaylistUser::moveEntryDown()
{
    quint64 videoPlaylistID;
    qint32 videoIndex;
    mSocketStream >> videoPlaylistID >> videoIndex;

    if (!mSocketStream.commitTransaction())
        return;

    if (!mIsAdmin)
        return;

    mPlaylist.moveVideoDownByID(videoPlaylistID, videoIndex);
}*/
