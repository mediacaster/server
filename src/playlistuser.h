#ifndef PLAYLISTUSER_H
#define PLAYLISTUSER_H

#include <QString>
#include <QTcpSocket>
#include <QDataStream>
#include <playlist.h>
#include <playercontroller.h>
#include <message.h>
#include <checkalivereply.h>

class PlaylistUser : public QObject
{
    Q_OBJECT

public:
    PlaylistUser(QTcpSocket& socket, PlayerController& playerController, Playlist& playlist, QString id, QString name, bool isAdmin, QObject* parent = nullptr);

    QString getID() const;
    QString getName() const;
    bool isAdmin() const;

    template<typename... Types>
    void sendMessage(Message::Header header, Types const&... content)
    {
        Message::writeMessage(mSocketStream, header, content...);
    }

    void kick(Message::Header kickMsg = Message::Header::CLIENT_KICKED);

    /**
     * @brief Checks if the user connection is still alive by sending a ping message
     * @return A CheckAliveReply enabling to be asynchronously notified of the check result
     *
     * Connect to the <alive> and <dead> signals of the reply to be notified of the check result.
     * Please note that the reply returned is dynamically allocated and must threfore be manually deleted.
     */
    CheckAliveReply* checkAlive();

signals:
    void requestUserKick(QString const& userName);
    void requestUserBan(QString const& userName);
    void userVoteToSkipCurrentEntry(QString const& userName, unsigned long currentEntryID);
    void disconnected();

private slots:
    void onReadyRead();

private:
    QTcpSocket& mSocket;
    QDataStream mSocketStream;
    PlayerController& mPlayerController;
    Playlist& mPlaylist;
    QString mID;
    QString mName;
    bool mIsAdmin;

    struct PlaylistEntryReference {
        quint64 mEntryID;
        qint32 mEntryIndex;

        PlaylistEntryReference(QDataStream& in)
        {
            in >> mEntryID >> mEntryIndex;
        }
    };

    bool checkCurrentEntry(quint64 currentEntryID);
    void connectToEnqueueReply(PlaylistEnqueueReply* enqueueReply);

    void enqueueMediaURL();
    void enqueueMediaID();
    void removeEntry();
    void kickUser();
    void banUser();
    void playEntry();
    void voteToSkipCurrent();
    void resumePlayback();
    void pausePlayback();
    void seekTo();
    void skipNext();
    void skipPrevious();
    void changeVolume();
    /*void moveEntryUp();
    void moveEntryDown();*/
};

#endif // PLAYLISTUSER_H
