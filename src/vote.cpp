#include "vote.h"

Vote::Vote(float passThreshold, QObject* parent) : QObject(parent), mFavorableVoters(), mPassThreshold(passThreshold), mVoterCount(0), mNeededFavorableVotes(-1)
{
    Q_ASSERT(0.0f <= passThreshold && passThreshold <= 1.0f);
}

void Vote::openVote(int voterCount)
{
    Q_ASSERT(voterCount >= 0);
    Q_ASSERT(!isOpen());

    setVoterCount(voterCount);
}

void Vote::voteInFavor(QString const& voterName)
{
    if (!isOpen())
        return;

    mFavorableVoters.insert(voterName);

    int favorableVotes = mFavorableVoters.size();

    emit voteUpdated(favorableVotes, mNeededFavorableVotes);

    checkVote();
}

void Vote::addVoter()
{
    if (!isOpen())
        return;

    setVoterCount(mVoterCount + 1);
}

void Vote::removeVoter(QString const& voterName)
{
    if (!isOpen())
        return;

    mFavorableVoters.remove(voterName);

    setVoterCount(mVoterCount - 1);
}

void Vote::closeVote()
{
    mNeededFavorableVotes = -1;
    mFavorableVoters.clear();
}

bool Vote::isOpen() const
{
    return mNeededFavorableVotes != -1;
}

void Vote::checkVote()
{
    if (mFavorableVoters.size() < mNeededFavorableVotes)
        return;

    closeVote();
    emit votePassed();
}

void Vote::setVoterCount(int voterCount)
{
    mVoterCount = voterCount;
    mNeededFavorableVotes = static_cast<int>(std::ceil(voterCount * mPassThreshold));

    emit voteUpdated(mFavorableVoters.size(), mNeededFavorableVotes);

    checkVote();
}
