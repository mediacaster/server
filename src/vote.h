#ifndef VOTE_H
#define VOTE_H

#include <QObject>
#include <QSet>

class Vote : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Create a new binary vote (yes/no vote)
     * @param passThreshold Minimum ratio of favorable votes over the number of voters
     *                      to pass the vote (inclusive).
     */
    Vote(float passThreshold, QObject* parent = nullptr);

    void openVote(int voterCount);

    void voteInFavor(QString const& voterName);

    void addVoter();

    void removeVoter(QString const& voterName);

    void closeVote();

    bool isOpen() const;

signals:
    void voteUpdated(int favorableVotes, int neededFavorableVotes);

    void votePassed();

private:
    QSet<QString> mFavorableVoters;
    float mPassThreshold;
    int mVoterCount;
    int mNeededFavorableVotes;

    void checkVote();

    void setVoterCount(int voterCount);
};

#endif // VOTE_H
